<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<c:choose>
    <c:when test="${empty sessionScope.user.role.name}">
        <%@ include file="/jsp/nav-bar.jsp" %>
    </c:when>
    <c:when test="${sessionScope.user.role.name == 'admin' or sessionScope.user.role.name == 'client'}">
        <%@ include file="/jsp/nav-bar-admin.jsp" %>
    </c:when>
</c:choose>