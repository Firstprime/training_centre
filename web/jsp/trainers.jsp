<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />

<html>
<head>
    <title><fmt:message key="title.trainers"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>

<jsp:useBean id="allTrainers" scope="request" type="java.util.List"/>

<main>
    <hr/>
    <table border="1px" align="center">
        <tr>
            <th colspan="2" ><p align="center"><fmt:message key="body.ourTrainers"/> </p></th>
        </tr>
        <c:forEach items="${allTrainers}" var="trainer">
            <tr>
                    <%--<td><input type="radio" value="${trainer.id}" name="trainerId" /></td>--%>
                <td><img src="${pageContext.request.contextPath}/image/avatar/cheburator.gif"></td>
                <td>${trainer.name} ${trainer.lastName}</td>
            </tr>
        </c:forEach>
    </table>
</main>
<footer class="footer">
    <%@include file="footer.jsp" %>
</footer>
</body>
</html>
