<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />
<html>
<head>
    <title><fmt:message key="title.signUp"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@include file="/jsp/nav-bar.jsp" %>
<hr><p align="center"><fmt:message key="body.enterFullData"/></p>
<main class="container">
    <div align="center">
        <form name="addUser" method="post" action="../controller">
            <input type="hidden" name="command" value="signup">
            <fmt:message key="user.fname"/>:<br/>
            <input type="text" name="name" title="<fmt:message key="title.form.name"/>" tabindex="1" required><br/>
            <fmt:message key="user.lname"/>:<br/>
            <input type="text" name="lastName" title="<fmt:message key="title.form.lastName"/>" required><br/>
            <fmt:message key="user.login"/>:<br/>
            <input type="text" name="email" maxlength="45" title="<fmt:message key="title.form.login"/>"
                   pattern="\w+@\w+\.[a-z]{2,4}" required><br/>
            <fmt:message key="label.password"/>:<br/>
            <input type="password" name="password" maxlength="45" title="<fmt:message key="title.form.pass"/>"
                   pattern="^(?=.*[A-Za-z]|(?=.*\d))[A-Za-z\d]{8,45}$" required><br/>
            <fmt:message key="label.password.confirmation"/>:<br/>
            <input type="password" name="confirm" maxlength="40" title="<fmt:message key="title.form.confirm"/>"
                   pattern="^(?=.*[A-Za-z]|(?=.*\d))[A-Za-z\d]{8,45}$" required><br/><br/>
            <button type="submit"><fmt:message key="button.submit"/></button>
            <br><br>
            <p style="color: red;">${requestScope.errorMessage}</p>
            ${sessionScope.wrongAction}<br/>
            ${sessionScope.nullPage}<br/>
        </form>
    </div>

    <div>
        <a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="body.link.backToMainPage"/></a>
    </div>
</main>
<footer class="footer">
    <%@include file="/jsp/footer.jsp" %>
</footer>
</body>
</html>
<%--<script>--%>
    <%--$('#field').setCustomValidity(text);--%>
<%--</script>--%>
