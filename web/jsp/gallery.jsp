<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />

<html>
<head>
    <title><fmt:message key="title.gallery"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/gallerystyle.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main class="container" style="padding-left: 0;padding-right: 0;">
<!-- Container for the image gallery -->
<%--<div class="container">--%>

    <!-- Full-width images with number text -->
    <div class="mySlides">
        <div class="numbertext">1 / 6</div>
        <img src="<c:url value="/image/gallery/girl-barbell.jpg"/>" style="width:100%">
    </div>

    <div class="mySlides">
        <div class="numbertext">2 / 6</div>
        <img src="<c:url value="/image/gallery/man-torso.jpg"/>" style="width:100%">
    </div>

    <div class="mySlides">
        <div class="numbertext">3 / 6</div>
        <img src="<c:url value="/image/gallery/body-torso.jpg"/>" style="width:100%">
    </div>

    <div class="mySlides">
        <div class="numbertext">4 / 6</div>
        <img src="<c:url value="/image/gallery/fitness-model-workout.jpg"/>" style="width:100%">
    </div>

    <div class="mySlides">
        <div class="numbertext">5 / 6</div>
        <img src="<c:url value="/image/gallery/fitness-model-workout-2.jpg"/>" style="width:100%">
    </div>

    <div class="mySlides">
        <div class="numbertext">6 / 6</div>
        <img src="<c:url value="/image/gallery/main-fitness-back.jpg"/>" style="width:100%">
    </div>

    <!-- Next and previous buttons -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <!-- Image text -->
    <div class="caption-container">
        <p id="caption"></p>
    </div>

    <!-- Thumbnail images -->
    <div class="row">
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/girl-barbell.jpg"/>" style="width:100%" onclick="currentSlide(1)" alt="">
        </div>
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/man-torso.jpg"/>" style="width:100%" onclick="currentSlide(2)" alt="">
        </div>
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/body-torso.jpg"/>" style="width:100%" onclick="currentSlide(3)" alt="">
        </div>
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/fitness-model-workout.jpg"/>" style="width:100%" onclick="currentSlide(4)" alt="">
        </div>
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/fitness-model-workout-2.jpg"/>" style="width:100%" onclick="currentSlide(5)" alt="">
        </div>
        <div class="column">
            <img class="demo cursor" src="<c:url value="/image/gallery/main-fitness-back.jpg"/>" style="width:100%" onclick="currentSlide(6)" alt="">
        </div>
    </div>
<%--</div>--%>
</main>
<%@include file="footer.jsp" %>

</body>
</html>

<script>
    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
    }
</script>