<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${locale eq 'ru'}">
        <fmt:setLocale value="ru_RU" scope="session"/>
    </c:when>
    <c:when test="${locale eq 'en'}">
        <fmt:setLocale value="en_EN" scope="session"/>
    </c:when>
</c:choose>
<fmt:setBundle basename="pagetext"/>

<nav class="navbar navbar-inverse" style="background: linear-gradient(to top, rgb(0,0,0) 31%, rgba(34,108,239,0.9) 100%);
                                   border: 0; border-radius: 0;margin-bottom: 0;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/index.jsp"/>"><span style="font-size: 30px; color: #226cef">Kachalocka</span></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/jsp/about.jsp" ><fmt:message
                        key="header.aboutUs"/></a></li>
                <li><a href="#" onclick="document.getElementById('trainers').submit(); return false;"><fmt:message
                        key="header.trainers"/></a>
                    <form id="trainers" method="get" action="../../controller">
                        <input type="hidden" name="command" value="showalltrainers"/>
                    </form>
                </li>
                <li><a href="${pageContext.request.contextPath}/jsp/gallery.jsp"><fmt:message key="header.gallery"/></a>
                </li>
                <li>
                    <a href="#" onclick="document.getElementById('reviews').submit(); return false;"><fmt:message
                            key="header.reviews"/></a>
                    <form id="reviews" method="get" action="../../controller">
                        <input type="hidden" name="command" value="showallreviews"/>
                    </form>
                </li>
                <li><a href="${pageContext.request.contextPath}/jsp/error/contacts.jsp"><fmt:message key="header.contacts"/></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/jsp/addUser.jsp">
                    <span class="glyphicon glyphicon-user"></span><fmt:message key="header.signUp"/></a></li>
                <li><a href="${pageContext.request.contextPath}/jsp/login.jsp">
                    <span class="glyphicon glyphicon-log-in"></span><fmt:message key="header.login"/></a></li>
                <li>
                    <style>
                        .langSelector {
                            background-color: cornflowerblue;
                        }
                    </style>
                    <jsp:include page="/jsp/lang-selector.jsp">
                        <jsp:param name="styleClass" value="langSelector"/>
                    </jsp:include>
                </li>
            </ul>
        </div>
    </div>
</nav>
