<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagetext"/>

<form name="changeLang" method="post" action="../../controller">
    <input type="hidden" name="command" value="changeLang">
    <select id="lang" name="lang" class="form-control ${param.styleClass}"
            title="<fmt:message key="header.title.lang"/>"
            onchange="this.form.submit()"
            style="margin: 10px; width: 100px; height:30px;">
        <option <c:if test="${pageContext.session.getAttribute('locale') eq 'en'}">selected</c:if> value="Eng"><fmt:message key="lang.en"/></option>
        <option <c:if test="${pageContext.session.getAttribute('locale') eq 'ru'}">selected</c:if> value="Ru"><fmt:message key="lang.ru"/></option>
        <option <c:if test="${pageContext.session.getAttribute('locale') eq 'ns'}">selected</c:if> value="Ns" title="not seriously">Ns</option>
    </select>
</form>
