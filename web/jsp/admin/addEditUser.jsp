<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<c:if test="${not empty addEditTarget and not empty addEditTarget.id and addEditTarget.id !=0}" var="editMode"/>
<html>
<head>

    <title><c:out value="${editMode ? 'Edit user' : 'Create user'}"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>

<%@ include file="/jsp/condition-for-header.jsp" %>

<main class="container">
    <%--<div align="center">--%>
    <div>
        <form name="addEditUser" method="post" action="../../controller">
            <table border="1px">
                <tr>
                    <th><fmt:message key="user.login"/></th>
                    <th><fmt:message key="user.fname"/></th>
                    <th><fmt:message key="user.lname"/></th>
                    <th><fmt:message key="user.discount"/></th>
                    <th><fmt:message key="user.roles"/></th>
                </tr>
                <tr>
                    <td><input type="text" name="email" value="${addEditTarget.login}"/></td>
                    <td><input type="text" name="name" value="${addEditTarget.name}"/></td>
                    <td><input type="text" name="lastName" value="${addEditTarget.lastName}"/></td>
                    <td><input type="number" name="discount" min="0" max="1" step="0.05"
                               value="${addEditTarget.discount}"/></td>
                    <td>${addEditTarget.role.name}</td>
                </tr>
            </table>
            <input type="hidden" name="userId" value="${addEditTarget.id}">
            <%--<center>--%>
            <br>
            <div>
                <div>
                    <label>
                        <input type="checkbox" id="changePasswordCheckbox" name="changePassword">
                        <fmt:message key="message.confirm.changePassword"/>
                    </label>
                    <p style="color: red;">${requestScope.errorMessage}</p>
                    <%--<br>--%>
                    <div hidden id="passwords">
                        <input type="password" name="pwd" placeholder=<fmt:message key="label.password"/>><br>
                        <input type="password" name="pwdConf" placeholder=<fmt:message
                                key="label.password.confirmation"/>>
                    </div>
                </div>
                <div>
                    <c:if test="${not empty roles}">
                        <label>
                            <input type="checkbox" id="changeRoleCheckbox" name="changeRole">
                            <fmt:message key="message.confirm.changeRole"/>
                        </label>
                        <%--<br>--%>
                        <select hidden id="roleSelector" name="newRole">
                            <c:forEach items="${roles}" var="role">
                                <c:if test="${role.id ne 1}">
                                    <option value="${role.id}"
                                            <c:if test="${role.id eq addEditTarget.role.id}">selected</c:if> >
                                            ${role.name}
                                    </option>
                                </c:if>
                            </c:forEach>
                                <%--todo IBW01: site debugger shows empty newRoleName --%>
                            <input type="hidden" name="newRoleName" value="${role.name}">
                        </select>
                    </c:if>
                </div>
            </div>
            <br><br>
            <%--</center>--%>
            <c:choose>
                <c:when test="${editMode}">
                    <input type="hidden" name="command" value="updateUser">
                    <input type="submit" value="<fmt:message key='button.user.update'/>">
                </c:when>
                <c:otherwise>
                    <input type="hidden" name="command" value="createUser">
                    <input type="submit" value="<fmt:message key='button.user.create'/>">
                </c:otherwise>
            </c:choose>
        </form>
    </div>
        <br><br>
    <div>
        <a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="body.link.backToMainPage"/></a>
    </div>
</main>
<footer class="footer">
    <%@include file="/jsp/footer.jsp" %>
</footer>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        $("#changePasswordCheckbox").change(function () {
            var div = $("#passwords");
            this.checked ? div.show() : div.hide();
        });
        $("#changeRoleCheckbox").change(function () {
            var form = $("#roleSelector");
            this.checked ? form.show() : form.hide();
        });
    });
</script>