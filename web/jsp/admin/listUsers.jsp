<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<form id="listUsersForm" method="post" action="../../controller">
    <table border="1px">
        <tr>
            <th></th>
            <th><fmt:message key="user.login"/></th>
            <th><fmt:message key="user.fname"/></th>
            <th><fmt:message key="user.lname"/></th>
            <th><fmt:message key="user.discount"/></th>
            <th><fmt:message key="user.roles"/></th>
        </tr>
        <c:forEach items="${allUsers}" var="user">
            <tr>
                <td><input type="radio" value="${user.id}" name="userId" /></td>
                <td>${user.login}</td>
                <td>${user.name}</td>
                <td>${user.lastName}</td>
                <td>${user.discount}</td>
                <td>${user.role.name}</td>
            </tr>
        </c:forEach>
    </table>
    <input type="hidden" id="listUsersForm-command" name="command" />

    <input type="button" id="editUserSubmitBtn" value="<fmt:message key="button.user.update"/>"/>
    <input type="button" id="deleteUserSubmitBtn" value="<fmt:message key="button.user.delete"/>"/>
</form>

<br>
<form method="post" action="../../controller">
    <input type="hidden" name="command" value="addEditUser"/>
    <input type="submit" value="<fmt:message key="button.user.create"/>"/>
</form>

<script type="text/javascript">
    var isUserSelected = function() {
        var numOfSelectedIds = $("input[name=userId]:checked").length;
        if (numOfSelectedIds !== 1) {
            alert("<fmt:message key='message.choose.user'/>");
            return false;
        }
        return true;
    };

    var submitFormForCommand = function (command) {
        $("#listUsersForm-command").val(command);
        $("#listUsersForm").submit();
    };

    $(document).ready(function() {
        $("#editUserSubmitBtn").click(function(e) {
            if (isUserSelected()) {
                submitFormForCommand("addEditUser");
            }
        });

        $("#deleteUserSubmitBtn").click(function(e) {
            if (isUserSelected() && confirm("message.user.delete.confirmation")) {
                submitFormForCommand("deleteUser");
            }
        });
    });
</script>