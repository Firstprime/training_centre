<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctag" uri="customtag" %>
<fmt:setBundle basename="pagetext" />

<html lang="en">
<head>
    <title><fmt:message key="title.adminPage"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main class="container">
    <div class="wrapper">
        <div class="content">
            <ctag:greeting user="${sessionScope.user}"/>
            <br/>
            <form name="viewAllClients" method="get" action="../../controller">
                <input type="hidden" name="command" value="showallclients"/>
                <button type="submit"><fmt:message key="button.showAllClients"/></button>
                <br/>
            </form>
            role - ${sessionScope.user.role.name}<br/>
            login - ${sessionScope.user.login}<br/>

            <div align="center" >
                <input type="image" width="100%" src="${pageContext.request.contextPath}/image/header-gym.jpg"/>
            </div>
        </div>
        <div>
            <a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="body.link.backToMainPage"/></a>
        </div>
    </div>
</main>
<footer class="footer">
    <%@include file="/jsp/footer.jsp" %>
</footer>
</body>
</html>
