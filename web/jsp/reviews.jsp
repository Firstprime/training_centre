<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />

<html lang="en">
<head>
    <title><fmt:message key="title.review"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>

<jsp:useBean id="allReviews" scope="request" type="java.util.List"/>
<main>
    <hr/>
<table border="1px" align="center">
    <tr>
        <!-- <th></th> -->
        <th style="text-align: center"><fmt:message key="user.fname"/></th>
        <th style="text-align: center"><fmt:message key="user.lname"/></th>
        <th style="text-align: center"><fmt:message key="table.review"/></th>
        <th style="text-align: center"><fmt:message key="table.date"/></th>


    </tr>
    <c:forEach items="${allReviews}" var="trainer">
        <tr>
            <%--<td><input type="radio" value="${review.id}" name="reviewId" /></td>--%>
            <td>${trainer.user.name}</td>
            <td>${trainer.user.lastName}</td>
            <td>${trainer.review}</td>
            <td>${trainer.date}</td>

        </tr>
    </c:forEach>
</table>
</main>
<footer class="footer">
    <%@include file="footer.jsp" %>
</footer>
</body>
</html>