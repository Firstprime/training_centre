<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="pagetext"/>

<html>
<head>
    <title><fmt:message key="title.login"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@include file="nav-bar.jsp" %>
<hr>
<p align="center"><fmt:message key="body.enterLogPass"/></p>
<div align="center">
    <form name="loginForm" method="post" action="../controller">
        <input type="hidden" name="command" value="login"/>
        <fmt:message key="user.login"/>:<br/>
        <input tabindex="1" type="text" name="email" maxlength="45" title="<fmt:message key="title.form.login"/>"
               required><br/>
        <fmt:message key="label.password"/>:<br/>
        <input type="password" name="password" maxlength="45" title="<fmt:message key="title.form.pass"/>"
               pattern="^(?=.*[A-Za-z]|(?=.*\d))[A-Za-z\d]{8,45}$" required><br/><br/>
        <button type="submit"><fmt:message key="button.submit"/></button>
        <br><br>
    </form>
        <p style="color: red;">${requestScope.errorMessage}</p>
        ${requestScope.wrongAction}<br/>
        ${requestScope.nullPage}<br/>
</div>
<div>
    <a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="body.link.backToMainPage"/></a>
</div>
<footer class="footer">
    <%@include file="footer.jsp" %>
</footer>
</body>
</html>
