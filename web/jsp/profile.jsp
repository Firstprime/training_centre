<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctag" uri="customtag" %>
<fmt:setBundle basename="pagetext"/>
<html lang="en">
<head>
    <title><fmt:message key="title.profilePage"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main class="container">
    <h3 align="center">${user.name} <fmt:message key="body.profile"/></h3><br/>
    <div><ctag:greeting user="${user}"/></div>
    <br>
    <div class="btn-group profile-page">
        <button class="button" style="width: 100%">button.writeReview</button>
        <br/>
        <button class="button" style="width: 100%">button.showAppointment</button>
        <br/>
        <button class="button" style="width: 100%">button.addAvatar</button>
        <br/>
        <button class="button" style="width: 100%">button.showAbonement</button>
        <br/>
    </div>
    <br/><br/>
    <%--comment area--%>
    <div class="container">
        <form method="post" action="../controller">
            <div class="form-group">
                <input type="hidden" name="command" value="addReview">
                <label for="comment"> <fmt:message key="body.addComment"/>: </label>
                <textarea name="review" maxlength="1000" class="form-control" rows="5" id="comment"
                          placeholder="<fmt:message key="placeholder.addComment"/>"></textarea>
                <button type="submit"><fmt:message key="button.submit"/></button>
            </div>
        </form>
    </div>

    <br/><br/>
    <a href="${pageContext.request.contextPath}/jsp/welcome.jsp"><fmt:message key="body.link.backToMainPage"/></a>
</main>
<footer class="footer">
    <%@include file="footer.jsp" %>
</footer>
</body>
</html>
