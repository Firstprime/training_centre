<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />
<html>
<head>
    <title><fmt:message key="title.about"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<hr/>
<div class="container marketing">
    <p align="center"><fmt:message key="body.about.head"/></p><br/>
    <p>Приложение имитирует сайт фитнесс-центра.</p>
    <p>Система работает с пользователями разделёнными на несколько категорий(реализованы user и admin).
    Без авторизации пользователь может просмотреть список тренеров, отзывы, зарегистрироваться, менять язык интерфейса.
        После регистрации пользователь автоматически авторизируется со статусом "User".</p>
    User'у дополнительно доступен личный кабинет где он может поменять персональные данные.(Не реализовано)<br/>
    Admin'у дополнительно доступна вкладка Administrate где он может добавлять, удалять и изменять пользователей,<br/>
    пароли и роли.<hr>
    <p>Учётная запись с правами администратора - login: <span style="color: #6ab938;">gytko_family@tut.by</span>
        password: <span style="color: coral;">1234567890</span></p>
    <p>Учётная запись с правами клиента - login: <span style="color: #6ab938;">ihar_blinou@epam.com</span>
        password: <span style="color: coral;">qwertyuiop</span></p>


</div>
<footer class="footer">
<%@include file="footer.jsp" %>
</footer>
</body>
</html>
