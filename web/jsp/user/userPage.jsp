<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctag" uri="customtag" %>
<fmt:setBundle basename="pagetext" />

<html lang="en">
<head>
    <title><fmt:message key="title.userPage"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main class="container">
    <ctag:greeting user="${user}"/><br/><br/><br/>
    <a href="${pageContext.request.contextPath}/jsp/welcome.jsp"><fmt:message key="body.link.backToMainPage"/></a>
</main>
<footer class="footer">
    <%@include file="../../jsp/footer.jsp" %>
</footer>
</body>
</html>


<%--todo redundant page--%>