<!DOCTYPE html>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />

<html lang="en">
<head>
    <title><fmt:message key="title.error404"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main>
    <div align="center">
        <hr/>
        <div align="center">
            <fmt:message key="body.pageNotFound"/>
        </div>
        <img src="${pageContext.request.contextPath}/image/images.png"/>
    </div>
</main>
<footer class="footer">
    <%@ include file="/jsp/footer.jsp" %>
</footer>
</body>
</html>
