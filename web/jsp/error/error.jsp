<!DOCTYPE html>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagetext" />

<html lang="en">
<head>
    <title><fmt:message key="title.errorPage"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mainstyle.css">
</head>
<body>
<%@ include file="/jsp/condition-for-header.jsp" %>
<main>
    <div align="center">
        <p> <fmt:message key="body.error.hello"/> </p>
        <input type="image" src="${pageContext.request.contextPath}/image/images.png"/>
    </div>
    <div>
        URI: ${pageContext.errorData.requestURI} is failed <br/>
        Error code: ${pageContext.errorData.statusCode}<br/>
        Exception type is: ${pageContext.exception}<br/>
        Message: ${pageContext.exception.message}<br/>
        My Message: ${errorMessage}<br/>
    </div>
</main>

    <%@include file="/jsp/footer.jsp" %>

</body>
</html>
