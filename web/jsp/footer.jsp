<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<div class="container">
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p align="center"><fmt:message key="footer.copyright"/></p>
</div>
