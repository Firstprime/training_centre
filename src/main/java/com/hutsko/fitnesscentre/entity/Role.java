package com.hutsko.fitnesscentre.entity;

import java.util.Objects;

/**
 * Immutable class contain nested class <code>Builder</code> for creating new <code>Role</code>.
 * There is no setters to change existing role. You can only create new one.
 * Private constructor <code>Role()</code> prevents creating new <Cole>Person</Cole> with empty role.
 */
public class Role {
    private int id;
    private String name;
    private Role() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }
        Role role = (Role) o;
        return id == role.id &&
                Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static class Builder {
        /**
         * Use <code>new Role.Builder().withId().withName().build()</code> to create a new <code>Role</code>
         *
         * @throws IllegalStateException if <code>name</code> or <code>id</code> wasn't initialize.
         */
        private final Role instance = new Role();

        public Role build() {
            if (instance.id == 0 || instance.name == null) {
                throw new IllegalStateException("Role object must have id and name defined");
            }
            return instance;
        }

        public Builder withId(int id) {
            instance.id = id;
            return this;
        }

        public Builder withName(String name) {
            instance.name = name;
            return this;
        }
    }
}
