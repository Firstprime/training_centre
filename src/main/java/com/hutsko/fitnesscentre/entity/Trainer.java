package com.hutsko.fitnesscentre.entity;

import java.util.Objects;

public class Trainer extends Person{
    private float salary;

    public Trainer() {
    }

    public Trainer(int id, String name, String lastName, String login, String password, float salary, Role role) {
        super(id, name, lastName, login, password, role);
        this.salary = salary;
    }

    public int getId() {
        return super.getId();
    }

    public void setId(int id) {
        super.setId(id);
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        super.setName(name);
    }

    public String getLastName() {
        return super.getLastName();
    }

    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    public String getLogin() {
        return super.getLogin();
    }

    public void setLogin(String login) {
        super.setLogin(login);
    }

    public String getPassword() {
        return super.getPassword();
    }

    public void setPassword(String password) {
        super.setPassword(password);
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Role getRole() {
        return super.getRole();
    }

    public void setRole(Role role) {
        super.setRole(role);
    }

    @Override

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainer)) return false;
        if (!super.equals(o)) return false;
        Trainer trainer = (Trainer) o;
        return Float.compare(trainer.salary, salary) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), salary);
    }

    @Override
    public String toString() {
        return super.toString() +
                "Trainer{" +
                "salary=" + salary +
                '}';
    }
}
