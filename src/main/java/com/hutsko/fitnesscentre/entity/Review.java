package com.hutsko.fitnesscentre.entity;

import java.util.Date;
import java.util.Objects;

public class Review {
    private String review;
    private Date date;
    private User user;

    public Review() {
    }

    public Review(String review, Date date, User user) {
        this.review = review;
        this.date = date;
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;
        Review review1 = (Review) o;
        return Objects.equals(review, review1.review) &&
                Objects.equals(date, review1.date) &&
                Objects.equals(user, review1.user);
    }

    @Override
    public int hashCode() {

        return Objects.hash(review, date, user);
    }

    @Override
    public String toString() {
        return "Review{" +
                "review='" + review + '\'' +
                ", date=" + date +
                ", user=" + user +
                '}';
    }
}
