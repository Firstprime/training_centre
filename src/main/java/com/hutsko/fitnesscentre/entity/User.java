package com.hutsko.fitnesscentre.entity;

import java.util.Objects;

public class User extends Person{
    private int trainer;
    private float discount;


    public User() {
    }


    public User(int id, int trainer, String name, String lastName, int discount, String login, String password, Role role) {
        super(id, name, lastName, login, password, role);
        this.trainer = trainer;
        this.discount = discount;
    }

    public int getId() {
        return super.getId();
    }

    public void setId(int id) {
        super.setId(id);
    }

    public int getTrainer() {
        return trainer;
    }

    public void setTrainer(int trainer) {
        this.trainer = trainer;
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        super.setName(name);
    }

    public String getLastName() {
        return super.getLastName();
    }

    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getLogin() {
        return super.getLogin();
    }

    public void setLogin(String login) {
        super.setLogin(login);
    }

    public String getPassword() {
        return super.getPassword();
    }

    public void setPassword(String password) {
        super.setPassword(password);
    }

    public Role getRole() {
        return super.getRole();
    }

    public void setRole(Role role) {
        super.setRole(role);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return trainer == user.trainer &&
                Float.compare(user.discount, discount) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), trainer, discount);
    }

    @Override
    public String toString() {
        return super.toString() +
                "User{" +
                "trainer=" + trainer +
                ", discount=" + discount +
                '}';
    }
}

