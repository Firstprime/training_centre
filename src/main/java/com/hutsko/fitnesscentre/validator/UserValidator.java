package com.hutsko.fitnesscentre.validator;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.hutsko.fitnesscentre.command.Command.PARAM_PAGE_ERROR_MESSAGE;

public class UserValidator {
    private static final String EMAIL_REG_EXP = "(\\w{2,29}@\\w{2,10})\\.[a-z]{2,4}";
    private static final String PASS_REG_EXP = "[\\p{Punct}\\w]{8,45}";

    private static final String PARAM_PASSWORD = "pwd";
    private static final String PARAM_PASSWORD_CONFIRMATION = "pwdConf";

    public boolean isLogPassValid(String login, String password) {
        boolean isLogin = isLoginValid(login);
        boolean isPass = isPasswordValid(password);
        return isLogin && isPass;
    }

    public boolean isLoginValid(String login) {
        boolean isLogin = false;
        if (login != null && !login.isEmpty()) {
            Pattern loginPattern = Pattern.compile(EMAIL_REG_EXP);
            Matcher loginMatcher = loginPattern.matcher(login);
            isLogin = loginMatcher.matches();
        }
        return isLogin;
    }

    public boolean isPasswordValid(String password) {
        boolean isPass = false;
        if (password != null && !password.isEmpty()) {
            Pattern passPattern = Pattern.compile(PASS_REG_EXP);
            Matcher passMatcher = passPattern.matcher(password);
            isPass = passMatcher.matches();
        }
        return isPass;
    }

    public boolean isChangePasswordConfirmed(HttpServletRequest request){
        final String password = request.getParameter(PARAM_PASSWORD);
        final String passwordConfirmation = request.getParameter(PARAM_PASSWORD_CONFIRMATION);
        boolean answer = true;
        if (!isPasswordValid(password)) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "invalid password");
            answer = false;
        }
        if (!password.equals(passwordConfirmation)) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "password not confirmed");
            answer = false;
        }
    return answer;
    }
}
