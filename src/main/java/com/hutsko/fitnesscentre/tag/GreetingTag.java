package com.hutsko.fitnesscentre.tag;

import com.hutsko.fitnesscentre.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class GreetingTag extends TagSupport {
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int doStartTag() {
        if (pageContext.getSession().getAttribute("locale") == "ru") {
            Locale.setDefault(new Locale("ru", "RU"));
        } else if (pageContext.getSession().getAttribute("locale") == "en") {
            Locale.setDefault(new Locale("en", "EN"));
        }
        ResourceBundle bundle = ResourceBundle.getBundle("pagetext");
        String greeting;
        String role = null;
        try {
            role = user.getRole().getName();
        } catch (NullPointerException e) {
            /*do nothing*/
        }
        if (role != null) {
            if ("admin".equalsIgnoreCase(role)) {
                greeting = bundle.getString("body.greeting.admin") + " " + user.getName();
            } else {
                greeting = bundle.getString("body.greeting") + " " + user.getName();
            }
        } else {
            greeting = bundle.getString("body.greeting");
        }
        try {
            pageContext.getOut().write(greeting);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

}
