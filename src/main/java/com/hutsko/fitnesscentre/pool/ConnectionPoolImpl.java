package com.hutsko.fitnesscentre.pool;

import com.hutsko.fitnesscentre.exception.DAOConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public final class ConnectionPoolImpl<T extends Connection> extends AbstractConnectionPool<T> implements ConnectionPool<T> {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolImpl.class);
    private static ResourceBundle resource = ResourceBundle.getBundle("mysql_config");
    private static final int POOL_SIZE = Integer.parseInt(resource.getString("db.poolsize"));
    private static final ConnectionPoolImpl<ConnectionProxy> POOL = new ConnectionPoolImpl<>(
            new ConnectionProxyValidator(), ConnectionFactory.getInstance());
    private BlockingQueue<T> connections;
    private Validator<T> validator;

    private ConnectionPoolImpl(Validator<T> validator, PoolContentFactory<T> factory) {
        this.validator = validator;
        try {
            // todo IBW01: Driver registration cannot be carried out in static block
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            connections = new ArrayBlockingQueue<>(POOL_SIZE);
            for (int i = 0; i < POOL_SIZE; i++) {
                connections.offer(factory.createNew());
            }
        } catch (SQLException e) {
            LOGGER.fatal(e.getMessage(), e);
            throw new RuntimeException("Driver registration or data access error has occurred. Application stopped. ", e);
        }
    }

    public static ConnectionPoolImpl<ConnectionProxy> getInstance() {
        return POOL;
    }

    @Override
    public T getConnection() throws DAOConnectionException {
        T connection = null;
        try {
            connection = connections.poll(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (connection == null) {
            throw new DAOConnectionException("Not enough connections in the pool");
        }
        return connection;
    }

    @Override
    protected boolean isValid(T connection) {
        return validator.isValid(connection);
    }

    /**
     * This method uses implicitly while <code>returnToPool()</code> invoked.
     * Check for <code>null</code> redundant because <code>isValid()</code> do it before this method call.<p>
     *
     * @param connection - may be <code>null</code> if invoked directly.
     */
    @Override
    protected void returnToPool(T connection) {
        connections.offer(connection);
    }

    @Override
    protected void handleFailedReturn(T connection) {
        // todo create new connection if old not valid
        LOGGER.error("Connection wasn't return to pool. Connections in pool currently: " + getPoolSize());
    }

    @Override
    public void terminate() {
        for (T connection : connections) {
            try {
                validator.invalidate(connection);
            } catch (DAOConnectionException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public int getPoolSize() {
        return connections.size();
    }
}