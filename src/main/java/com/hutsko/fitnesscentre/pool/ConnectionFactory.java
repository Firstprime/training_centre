package com.hutsko.fitnesscentre.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionFactory implements PoolContentFactory<ConnectionProxy> {
    private static ResourceBundle resource = ResourceBundle.getBundle("mysql_config");
    private static final ConnectionFactory FACTORY = new ConnectionFactory(resource);
    private String url;
    private String user;
    private String pass;

    private ConnectionFactory(ResourceBundle resource) {
        this.url = resource.getString("db.url");
        this.user = resource.getString("db.username");
        this.pass = resource.getString("db.password");
    }

    public static ConnectionFactory getInstance(){
        return FACTORY;
    }

    /**
     * This method initialize new <code>Connection</code> and wrap it to <code>ConnectionProxy</code>.
     * @return <code>ConnectionProxy</code>.
     * @throws SQLException if connection creation was failed.
     */
    @Override
    public ConnectionProxy createNew() throws SQLException {
        Connection connection;
        ConnectionProxy connectionProxy;
        try {
            connection = DriverManager.getConnection(url, user, pass);
            connectionProxy = new ConnectionProxy(connection);
        } catch (SQLException e) {
            throw new SQLException("Cannot create new connection. Check the properties for your database");
        }
        return connectionProxy;
    }
}
