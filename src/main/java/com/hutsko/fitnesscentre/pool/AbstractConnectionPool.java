package com.hutsko.fitnesscentre.pool;

public abstract class AbstractConnectionPool <T> implements ConnectionPool<T> {

    @Override
    public final void returnConnection(T connection) {
        if (isValid(connection)) {
            returnToPool(connection);
        } else {
            handleFailedReturn(connection);
        }
    }

    protected abstract boolean isValid(T connection);
    protected abstract void returnToPool(T connection);
    protected abstract void handleFailedReturn(T connection);
}
