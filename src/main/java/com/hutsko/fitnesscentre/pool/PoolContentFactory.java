package com.hutsko.fitnesscentre.pool;

import java.sql.SQLException;

/**
 * Define a mechanism to create new objects for a pool.
 * @param <T> - type of object to create.
 */
public interface PoolContentFactory<T> {
    T createNew() throws SQLException;
}
