package com.hutsko.fitnesscentre.pool;

import com.hutsko.fitnesscentre.exception.DAOConnectionException;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;

public interface ConnectionPool<T> {
    /**
     * Retrieve an object from the pool or <code>null</code> if there is no objects during timeout.
     * <p>
     * The validity of objects is guaranteed by using nested interface {@link Validator}.
     * @return T - one of pooled object.
     */
    T getConnection() throws DAOTechnicalException;

    /**
     * Put an object back to the pool.
     * @param connection - the object to return to the pool.
     */
    void returnConnection(T connection);

    /**
     * This method releases all resources by call another method  <code>invalidate(T connection)</code>
     * from nested interface {@link Validator}, and then shuts down the pool.
     * <p>
     * Should be called at the end of the application.
     */
    void terminate();

    /**
     * Validate an objects of the pool.
     * @param <T> the type of objects to validate.
     */
    interface Validator<T> {

        /**
         * Check if object is valid.
         * @param connection - the object for validation.
         * @return <code>true</code> for valid objects and <code>false</code> for invalid objects.
         */
        boolean isValid(T connection);

        /**
         * This method release the memory from unnecessary objects
         *
         * @param connection - the object to cleanup
         */
        void invalidate(T connection) throws DAOConnectionException;
    }
}
