package com.hutsko.fitnesscentre.pool;

import com.hutsko.fitnesscentre.exception.DAOConnectionException;
import com.hutsko.fitnesscentre.pool.ConnectionPool.Validator;

import java.sql.SQLException;

public class ConnectionProxyValidator implements Validator<ConnectionProxy> {
    @Override
    public boolean isValid(ConnectionProxy connection) {
        return connection != null && !connection.isClosed();
    }

    @Override
    public void invalidate(ConnectionProxy connection) throws DAOConnectionException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DAOConnectionException("Closing connection error", e);
        }
    }
}
