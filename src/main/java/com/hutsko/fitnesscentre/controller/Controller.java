package com.hutsko.fitnesscentre.controller;

import com.hutsko.fitnesscentre.command.ActionFactory;
import com.hutsko.fitnesscentre.command.Command;
import com.hutsko.fitnesscentre.command.EmptyCommand;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.hutsko.fitnesscentre.command.Command.PARAM_PAGE_ERROR_MESSAGE;
import static com.hutsko.fitnesscentre.command.Command.PATH_PAGE_ERROR;

@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    private static final String PATH_INDEX_PAGE = "/index.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String param = req.getParameter("command");
        String page;
        try {
            Optional<Command> commandOptional = ActionFactory.defineCommand(param);
            Command command = commandOptional.orElse(new EmptyCommand());
            page = command.execute(req);
            if (page != null) {
                if ("showalltrainers".equals(param) || "showallreviews".equals(param)) {
                    RequestDispatcher dispatcher = req.getRequestDispatcher(page);
                    dispatcher.forward(req, resp);
                } else {
                    resp.sendRedirect(page);
                }
            } else {
                req.setAttribute("nullPage", "empty page");
                resp.sendRedirect(req.getContextPath() + PATH_INDEX_PAGE);
            }
        } catch (Throwable e) {
            req.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Unknown error. Our incredibly stable site has broke. Call administrator!");
            LOGGER.fatal(e.getMessage(), e);
            RequestDispatcher dispatcher = req.getRequestDispatcher(PATH_PAGE_ERROR);
            dispatcher.forward(req, resp);
        }
    }

    @Override
    public void destroy() {
        ConnectionPoolImpl.getInstance().terminate();
    }

}
