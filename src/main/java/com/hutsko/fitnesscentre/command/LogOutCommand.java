package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogOutCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LogOutCommand.class);

    private static final String PATH_PAGE_INDEX = "/index.jsp";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        try {
            User user = (User) session.getAttribute("user");
            int userId = user.getId();
            session.invalidate();
            LOGGER.info("UserID " + userId + " session destroyed.");
        } catch (NullPointerException e) {
            LOGGER.info("Session " + request.getRequestedSessionId() + " expired.");
        }
        return request.getContextPath() + PATH_PAGE_INDEX;
    }
}
