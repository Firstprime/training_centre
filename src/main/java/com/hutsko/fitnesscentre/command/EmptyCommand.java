package com.hutsko.fitnesscentre.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
public class EmptyCommand implements Command {
        private static final Logger LOGGER = LogManager.getLogger(EmptyCommand.class);

        @Override
    public String execute(HttpServletRequest request) {
            LOGGER.warn("empty command executed");
        return null;
    }
}
