package com.hutsko.fitnesscentre.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ChangeLangCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(ChangeLangCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        final String CURRENT_PATH = request.getRequestURI();
        String lang = request.getParameter("lang");

        switch (lang) {
            case "Ru":
                request.getSession().setAttribute("locale", "ru");
                break;
            case "Eng":
                request.getSession().setAttribute("locale", "en");
                break;
            default:
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Language not supported");
                LOGGER.warn("Illegal language state.");
                return PATH_PAGE_ERROR;
        }
        return CURRENT_PATH;
    }
}
