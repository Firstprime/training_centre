package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AddEditUserCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AddEditUserCommand.class);

    private static final String PARAM_USER_ID = "userId";

    private static final String ATTRIBUTE_USER = "addEditTarget";
    private static final String ATTRIBUTE_ROLES = "roles";

    private static final String PATH_PAGE_ADD_EDIT_USER = "/jsp/admin/addEditUser.jsp";

    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {
        String page = PATH_PAGE_ADD_EDIT_USER;
        String userId = request.getParameter(PARAM_USER_ID);
        User addEditTarget = new User();

        if (userId != null) {
            try {
                addEditTarget.setId(Integer.parseInt(userId));
                addEditTarget = adminDAO.getUser(addEditTarget);
            } catch (UserAuthenticationException e) {
                page = PATH_PAGE_RESPONSE;
                LOGGER.warn("User not found. Cause: information could be outdated(user has been deleted)");
            } catch (DAOTechnicalException e) {
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "User not created/edited");
                LOGGER.error(e.getMessage(), e);
                page = PATH_PAGE_ERROR;
            }
        } else {
            addEditTarget.setDiscount(1);
        }
        request.getSession().setAttribute(ATTRIBUTE_USER, addEditTarget);
        try {
            List<Role> roles = adminDAO.getRoles();
            request.getSession().setAttribute(ATTRIBUTE_ROLES, roles);
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "User not created/edited");
            LOGGER.error(e.getMessage(), e);
            page = PATH_PAGE_ERROR;
        }
        return page;
    }
}
