package com.hutsko.fitnesscentre.command;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface define method <code>execute(HttpServletRequest request)</code>
 * and constants common to all commands.
 */
public interface Command {

    String PARAM_EMAIL = "email";
    String PARAM_PASSWORD = "password";
    String PARAM_NEW_ROLE_ID = "newRole";
    String PARAM_NEW_ROLE_NAME = "newRoleName";
    String CHECKBOX_CHECKED_VALUE = "on";
    String PARAM_PAGE_ERROR_MESSAGE = "errorMessage";

    String PATH_PAGE_RESPONSE = "/jsp/admin/response.jsp";
    String PATH_PAGE_ADD_EDIT_USER = "/jsp/admin/addEditUser.jsp";
    String PATH_PAGE_ERROR = "/jsp/error/error.jsp";
    String PATH_PAGE_ERROR_404 = "/jsp/error/error404.jsp";

    String execute(HttpServletRequest request);
}
