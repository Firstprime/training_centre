package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.CommonDAO;
import com.hutsko.fitnesscentre.dao.CommonDAOImpl;
import com.hutsko.fitnesscentre.entity.Trainer;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowAllTrainersCommand implements Command{
    private static final Logger LOGGER = LogManager.getLogger(ShowAllTrainersCommand.class);

    private static final String PATH_PAGE_TRAINERS = "/jsp/trainers.jsp";

    private CommonDAO commonDAO = CommonDAOImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request){
        List<Trainer> trainers;
        try {
            trainers = commonDAO.showAllTrainers();
            request.setAttribute("allTrainers", trainers);
        } catch (DAOTechnicalException e) {
            LOGGER.error(e.getMessage(), e);
            return PATH_PAGE_ERROR;
        }
        return PATH_PAGE_TRAINERS;
    }
}
