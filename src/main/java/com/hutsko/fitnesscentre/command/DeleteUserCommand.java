package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.hutsko.fitnesscentre.command.UserAction.PARAM_ID;
import static com.hutsko.fitnesscentre.command.UserAction.PATH_LIST_USERS;

public class DeleteUserCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DeleteUserCommand.class);

    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {

        final User user = new User();
        user.setId(Integer.parseInt(request.getParameter(PARAM_ID)));
        try {
            adminDAO.deleteUser(user);
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "User not deleted");
            LOGGER.error(e.getMessage(), e);
            return PATH_PAGE_ERROR;
        }
        return PATH_LIST_USERS;
    }
}
