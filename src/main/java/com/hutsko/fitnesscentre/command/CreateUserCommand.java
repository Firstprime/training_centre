package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.hutsko.fitnesscentre.command.UserAction.PATH_LIST_USERS;
import static com.hutsko.fitnesscentre.command.UserAction.fillUserAttributes;

public class CreateUserCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(CreateUserCommand.class);

    private static final String PARAM_SETUP_PWD = "changePassword";
    private static final String PARAM_SETUP_ROLE = "changeRole";
    private static final String PARAM_PASSWORD = "pwd";

    private UserValidator validator = new UserValidator();
    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {

        final String setupPwdParam = request.getParameter(PARAM_SETUP_PWD);
        final boolean passwordChangeRequired = CHECKBOX_CHECKED_VALUE.equals(setupPwdParam);
        final String setupRoleParam = request.getParameter(PARAM_SETUP_ROLE);
        final boolean roleChangeRequired = CHECKBOX_CHECKED_VALUE.equalsIgnoreCase(setupRoleParam);
        final String password = request.getParameter(PARAM_PASSWORD);
        final int newRoleId = Integer.parseInt(request.getParameter(PARAM_NEW_ROLE_ID));
        final String newRoleName = request.getParameter(PARAM_NEW_ROLE_NAME);
        Boolean changePassPermit = false;

        if (passwordChangeRequired) {
            if (!(changePassPermit = validator.isChangePasswordConfirmed(request))) {
                return PATH_PAGE_ADD_EDIT_USER;
            }
        }
        final User user = fillUserAttributes(request);
        if (!validator.isLoginValid(user.getLogin())) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Login invalid");
            return PATH_PAGE_ADD_EDIT_USER;
        }
        try {
            if (changePassPermit) {
                user.setPassword(password);
            }
            if (roleChangeRequired) {
                user.setRole(new Role.Builder().withName(newRoleName).withId(newRoleId).build());
            }
            adminDAO.addUser(user);
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Login occupied");
            LOGGER.error("Login occupied", e);
            return PATH_PAGE_ADD_EDIT_USER;
        }
        return PATH_LIST_USERS;
    }

}
