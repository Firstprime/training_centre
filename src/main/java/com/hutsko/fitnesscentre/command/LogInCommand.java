package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.CommonDAO;
import com.hutsko.fitnesscentre.dao.CommonDAOImpl;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;
import com.hutsko.fitnesscentre.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogInCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LogInCommand.class);

    private static final String PATH_PAGE_LOGIN = "/jsp/login.jsp";
    private static final String PATH_PAGE_USER_WELCOME = "/jsp/profile.jsp";
    private static final String PATH_PAGE_ADMIN_WELCOME = "/jsp/admin/adminPage.jsp";

    private UserValidator validator = new UserValidator();
    private CommonDAO commonDAO = CommonDAOImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request) {
        String page = PATH_PAGE_LOGIN;
        String loginValue = request.getParameter(PARAM_EMAIL);
        String passValue = request.getParameter(PARAM_PASSWORD);

        if (validator.isLogPassValid(loginValue, passValue)) {
            User user;
            try {
                user = (User) commonDAO.authenticateUser(loginValue, passValue);
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                switch (user.getRole().getName()) {
                    case "admin":
                        page = PATH_PAGE_ADMIN_WELCOME;
                        break;
                    case "client":
                        page = PATH_PAGE_USER_WELCOME;
                        break;
                }
            } catch (UserAuthenticationException e) {
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Invalid login or password");
                page = PATH_PAGE_LOGIN;
            } catch (DAOTechnicalException e) {
                page = PATH_PAGE_ERROR;
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Incorrect login or password.");
        }
        return page;
    }
}
