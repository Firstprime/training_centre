package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AddReviewCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AddReviewCommand.class);

    private static final String PARAM_COMMENT = "review";
    private static final String PATH_PAGE_ALL_REVIEWS = "/controller?command=showallreviews";

    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        final String comment = request.getParameter(PARAM_COMMENT);
        final int userId = user.getId();
        try {
            adminDAO.addComment(userId, comment);
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Comment wasn't added.");
            LOGGER.error("Action denied. Attempt to add comment duplicate.", e);
            return PATH_PAGE_ERROR;
        }
        return PATH_PAGE_ALL_REVIEWS;
    }
}
