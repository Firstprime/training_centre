package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.hutsko.fitnesscentre.command.UserAction.*;

public class UpdateUserCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UpdateUserCommand.class);

    private static final String PARAM_CHANGE_PWD = "changePassword";
    private static final String PARAM_PASSWORD = "pwd";
    private static final String PARAM_CHANGE_ROLE = "changeRole";

    private UserValidator validator = new UserValidator();
    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {

        final int userId = Integer.parseInt(request.getParameter(PARAM_ID));
        final boolean passwordChangeRequired = CHECKBOX_CHECKED_VALUE.equals(request.getParameter(PARAM_CHANGE_PWD));
        final String password = request.getParameter(PARAM_PASSWORD);
        final boolean roleChangeRequired = CHECKBOX_CHECKED_VALUE.equals(request.getParameter(PARAM_CHANGE_ROLE));
        final int newRoleId = Integer.parseInt(request.getParameter(PARAM_NEW_ROLE_ID));
        final String newRoleName = request.getParameter(PARAM_NEW_ROLE_NAME);
        final User user = fillUserAttributes(request);
        Boolean changePassPermit = false;

        user.setId(userId);
        if (passwordChangeRequired) {
            if (!(changePassPermit = validator.isChangePasswordConfirmed(request))) {
                return PATH_PAGE_ADD_EDIT_USER;
            }
        }
        try {
            if (!validator.isLoginValid(user.getLogin())) {
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Login invalid");
                return PATH_PAGE_ADD_EDIT_USER;
            }
            adminDAO.editUser(user);
            if (changePassPermit) {
                user.setPassword(password);
                adminDAO.updatePassword(user);
            }
            if (roleChangeRequired) {
                user.setRole(new Role.Builder().withId(newRoleId).withName(newRoleName).build());
                adminDAO.changeRole(user);
            }
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "User not changed.");
            LOGGER.error(e.getMessage(), e);
            return PATH_PAGE_ERROR;
        }
        return PATH_LIST_USERS;
    }
}
