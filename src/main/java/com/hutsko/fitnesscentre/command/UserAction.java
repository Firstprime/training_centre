package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.entity.User;

import javax.servlet.http.HttpServletRequest;

/**
 * Utility class. It contains methods for creating entities and common constants.<p>
 * Package-private access.
 */
/*package*/ class UserAction {

    /* non-instantiable class */
    private UserAction() {
    }

    /*package*/ static String PARAM_ID = "userId";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_DISCOUNT = "discount";
    private static final String PARAM_PASSWORD = "password";
    private static final int PARAM_DISCOUNT_DEFAULT = 1;

    /*package*/ static String PATH_LIST_USERS = "/controller?command=showallclients";

    /*package*/ static User fillUserAttributes(HttpServletRequest request) {
        final User user = new User();
        user.setName(request.getParameter(PARAM_NAME));
        user.setLastName(request.getParameter(PARAM_LAST_NAME));
        user.setLogin(request.getParameter(PARAM_EMAIL));
        user.setPassword(request.getParameter(PARAM_PASSWORD));
        if (request.getParameter(PARAM_DISCOUNT) == null) {
            /* if-else prevent NPE for parseFloat(null) */
            user.setDiscount(PARAM_DISCOUNT_DEFAULT);
        } else {
            user.setDiscount(Float.parseFloat(request.getParameter(PARAM_DISCOUNT)));
        }
        user.setRole(new Role.Builder().withId(4).withName("client").build());
        return user;
    }

}
