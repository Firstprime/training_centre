package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.CommonDAO;
import com.hutsko.fitnesscentre.dao.CommonDAOImpl;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;
import com.hutsko.fitnesscentre.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.hutsko.fitnesscentre.command.UserAction.fillUserAttributes;

public class SignUpCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(SignUpCommand.class);

    private static final String PARAM_CONFIRM = "confirm";
    private static final String PATH_PAGE_ADD_USER = "/jsp/addUser.jsp";
    private static final String PATH_PAGE_USER_WELCOME = "/jsp/user/userPage.jsp";

    private UserValidator validator = new UserValidator();
    private CommonDAO commonDAO = CommonDAOImpl.getInstance();

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String loginValue = request.getParameter(PARAM_EMAIL);
        String passValue = request.getParameter(PARAM_PASSWORD);
        String confirmationValue = request.getParameter(PARAM_CONFIRM);
        if (passValue.equals(confirmationValue)) {
            User user;
            try {
                /* todo: refactor this - main scenario works through throwing exception */
                user = (User) commonDAO.authenticateUser(loginValue, passValue);
            } catch (UserAuthenticationException e) {
                user = null;
            } catch (DAOTechnicalException e) {
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Internal problem.");
                LOGGER.error(e.getMessage(), e);
                return PATH_PAGE_ERROR;
            }
            if (validator.isLogPassValid(loginValue, passValue) && user == null) {
                user = fillUserAttributes(request);
                try {
                    commonDAO.addUser(user);
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);
                    page = PATH_PAGE_USER_WELCOME;
                } catch (DAOTechnicalException e) {
                    request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Internal problem.");
                    LOGGER.error(e.getMessage(), e);
                    return PATH_PAGE_ERROR;
                }
            } else {
                request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "This email is reserved. Try another email.");
                page = PATH_PAGE_ADD_USER;
            }
        } else {
            page = PATH_PAGE_ADD_USER;
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "password not confirmed.");
        }
        return page;
    }
}
