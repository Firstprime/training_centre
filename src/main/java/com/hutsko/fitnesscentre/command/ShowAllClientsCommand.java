package com.hutsko.fitnesscentre.command;

import com.hutsko.fitnesscentre.dao.AdminDAO;
import com.hutsko.fitnesscentre.dao.PersonDAO;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowAllClientsCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(ShowAllClientsCommand.class);

    private PersonDAO adminDAO = AdminDAO.getInstance();

    @Override
    public String execute(HttpServletRequest request) {
        List<User> list;
        try {
            list = adminDAO.getAllUsers();
            request.getSession().setAttribute("allUsers", list);
        } catch (DAOTechnicalException e) {
            request.getSession().setAttribute(PARAM_PAGE_ERROR_MESSAGE, "Internal problem");
            LOGGER.error(e.getMessage(), e);
            return PATH_PAGE_ERROR;
        }
        return PATH_PAGE_RESPONSE;
    }
}
