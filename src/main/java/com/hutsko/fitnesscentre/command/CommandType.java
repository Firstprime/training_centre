package com.hutsko.fitnesscentre.command;

public enum CommandType {
    LOGIN(new LogInCommand()),
    SIGNUP(new SignUpCommand()),
    LOGOUT(new LogOutCommand()),
    CHANGELANG(new ChangeLangCommand()),
    ADDEDITUSER(new AddEditUserCommand()),
    ADDREVIEW(new AddReviewCommand()),
    CREATEUSER(new CreateUserCommand()),
    UPDATEUSER(new UpdateUserCommand()),
    DELETEUSER(new DeleteUserCommand()),
    SHOWALLCLIENTS(new ShowAllClientsCommand()),
    SHOWALLREVIEWS(new ShowAllReviewsCommand()),
    SHOWALLTRAINERS(new ShowAllTrainersCommand());

    private Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public Command getCommand(){
        return command;
    }

}
