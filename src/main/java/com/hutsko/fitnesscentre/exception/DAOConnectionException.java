package com.hutsko.fitnesscentre.exception;

/**
 * This exception indicates a connection issue while processing "Training centre" application. E.g. driver registration
 * or <code>Connection</code> creating.
 */
public class DAOConnectionException extends DAOTechnicalException {
    public DAOConnectionException() {
        super();
    }

    public DAOConnectionException(String message) {
        super(message);
    }

    public DAOConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOConnectionException(Throwable cause) {
        super(cause);
    }
}
