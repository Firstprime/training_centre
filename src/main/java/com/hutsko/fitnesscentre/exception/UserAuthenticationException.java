package com.hutsko.fitnesscentre.exception;

/**
 * This exception thrown while user with certain login and password was not found in database
 */
public class UserAuthenticationException extends BaseFitnessException{
    public UserAuthenticationException() {
        super();
    }

    public UserAuthenticationException(String message) {
        super(message);
    }

    public UserAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAuthenticationException(Throwable cause) {
        super(cause);
    }
}
