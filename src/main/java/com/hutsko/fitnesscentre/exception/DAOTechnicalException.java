package com.hutsko.fitnesscentre.exception;

/**
 * This exception is a wrapper for the following:
 * <code>DAOConnectionException</code>, <code>DAOModificationException</code>
 */
public class DAOTechnicalException extends BaseFitnessException {
    public DAOTechnicalException() {
        super();
    }

    public DAOTechnicalException(String message) {
        super(message);
    }

    public DAOTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOTechnicalException(Throwable cause) {
        super(cause);
    }
}
