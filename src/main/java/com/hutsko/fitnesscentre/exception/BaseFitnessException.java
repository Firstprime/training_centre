package com.hutsko.fitnesscentre.exception;

/**
 * Super class of exceptions for "Training centre" application.
 */
public class BaseFitnessException extends Exception {
    public BaseFitnessException() {
    }

    public BaseFitnessException(String message) {
        super(message);
    }

    public BaseFitnessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseFitnessException(Throwable cause) {
        super(cause);
    }
}
