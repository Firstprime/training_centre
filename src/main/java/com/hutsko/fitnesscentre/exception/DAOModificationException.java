package com.hutsko.fitnesscentre.exception;

/**
 * This exception indicates a <code>SQLException</code> thrown while processing Create,
 * Update or Delete database operations within "Training centre" application.
 */
public class DAOModificationException extends DAOTechnicalException {
    public DAOModificationException() {
        super();
    }

    public DAOModificationException(String message) {
        super(message);
    }

    public DAOModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOModificationException(Throwable cause) {
        super(cause);
    }
}
