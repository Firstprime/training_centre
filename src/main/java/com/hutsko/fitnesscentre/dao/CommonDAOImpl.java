package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.*;
import com.hutsko.fitnesscentre.exception.DAOModificationException;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;
import com.hutsko.fitnesscentre.pool.AbstractConnectionPool;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImpl;
import com.hutsko.fitnesscentre.pool.ConnectionProxy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CommonDAOImpl implements CommonDAO {
    private static final Logger LOGGER = LogManager.getLogger(CommonDAOImpl.class);
    private static final CommonDAOImpl DAO = new CommonDAOImpl();

    private static final String COLUMN_ROLE_ID = "role_id";
    private static final String COLUMN_ROLE_NAME = "role_name";
    private static final String COLUMN_TRAINER_NAME = "trainer_f_name";
    private static final String COLUMN_TRAINER_L_NAME = "trainer_l_name";

    private static final String SQL_CHECK_USER = "SELECT r.role_id, r.role_name, c.client_f_name, c.client_id " +
            "FROM clients c LEFT JOIN role r ON c.role_id = r.role_id WHERE client_login = ? AND client_pass = sha1(?);";
    private static final String SQL_CREATE_CLIENT = "INSERT INTO clients (client_f_name, client_l_name, client_login,"
            + " client_pass, role_id) VALUES (?, ?, ?, sha1(?), 4);";
    private static final String SQL_GET_ALL_REVIEWS = "SELECT client_f_name, client_l_name, review, date FROM clients c,"
            + " reviews r WHERE c.client_id=r.client_id;";
    private static final String SQL_GET_ALL_TRAINERS = "SELECT trainer_id, trainer_f_name, trainer_l_name,"
            + " trainer_salary, role_id FROM trainers;";

    private AbstractConnectionPool<ConnectionProxy> pool;
    private ConnectionProxy connection;

    private CommonDAOImpl() {
        pool = ConnectionPoolImpl.getInstance();
    }

    public static CommonDAOImpl getInstance() {
        return DAO;
    }

    @Override
    public void addUser(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_CREATE_CLIENT);
            statement.setString(1, user.getName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOModificationException("User" + user.getLogin() + " not created", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public Person authenticateUser(String login, String pass) throws UserAuthenticationException, DAOTechnicalException {
        connection = pool.getConnection();
        User user;
        PreparedStatement pStatement = null;
        ResultSet resultSet;
        try {
            user = new User();
            pStatement = connection.prepareStatement(SQL_CHECK_USER);
            pStatement.setString(1, login);
            pStatement.setString(2, pass);
            resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
                user.setRole(new Role.Builder()
                        .withId(resultSet.getInt(COLUMN_ROLE_ID))
                        .withName(resultSet.getString(COLUMN_ROLE_NAME))
                        .build());
                user.setName(resultSet.getString(3));
                user.setLogin(login);
                user.setId(resultSet.getInt(4));
            }
            if (user.getName() == null) {
                throw new UserAuthenticationException("User is not found.");
            }
            LOGGER.info("User '" + user.getLogin() + "'is found.");
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL Exception(request or table failed).", e);
        } finally {
            closeStatement(pStatement);
            pool.returnConnection(connection);
        }
        return user;
    }

    @Override
    public List<Review> showAllReviews() throws DAOTechnicalException {
        connection = pool.getConnection();
        List<Review> reviews = new ArrayList<>();
        Statement statement = null;
        Review review;
        User user;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_REVIEWS);
            while (resultSet.next()) {
                user = new User();
                review = new Review();
                user.setName(resultSet.getString(1));
                user.setLastName(resultSet.getString(2));
                review.setReview(resultSet.getString(3));
                review.setDate(resultSet.getDate(4));
                review.setUser(user);
                reviews.add(review);
            }
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL Exception(request or table failed)", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
        return reviews;
    }

    @Override
    public List<Trainer> showAllTrainers() throws DAOTechnicalException {
        connection = pool.getConnection();
        List<Trainer> trainers = new ArrayList<>();
        Statement statement = null;
        Trainer trainer;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_TRAINERS);
            while (resultSet.next()) {
                trainer = new Trainer();
                trainer.setName(resultSet.getString(COLUMN_TRAINER_NAME));
                trainer.setLastName(resultSet.getString(COLUMN_TRAINER_L_NAME));
                trainers.add(trainer);
            }
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL Exception(request or table failed)", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
        return trainers;
    }

    private void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Statement not closed:" + e);
        }
    }
}
