package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.*;
import com.hutsko.fitnesscentre.exception.DAOModificationException;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;
import com.hutsko.fitnesscentre.pool.AbstractConnectionPool;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImpl;
import com.hutsko.fitnesscentre.pool.ConnectionProxy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
public class AdminDAO implements PersonDAO {
    private static final Logger LOGGER = LogManager.getLogger(AdminDAO.class);
    private static final AdminDAO DAO = new AdminDAO();

    private static final String COLUMN_ROLE_ID = "role_id";
    private static final String COLUMN_ROLE_NAME = "role_name";
    private static final String COLUMN_CLIENT_ID = "client_id";
    private static final String COLUMN_CLIENT_TRAINER_ID = "trainer_id";
    private static final String COLUMN_CLIENT_F_NAME = "client_f_name";
    private static final String COLUMN_CLIENT_L_NAME = "client_l_name";
    private static final String COLUMN_CLIENT_DISCOUNT = "client_discount";
    private static final String COLUMN_CLIENT_LOGIN = "client_login";

    private static final String SQL_FIND_ALL_CLIENTS = "SELECT * FROM clients c LEFT JOIN role r ON r.role_id=c.role_id"
            + " ORDER BY r.role_name, c.client_id;";
    private static final String SQL_GET_CLIENT_BY_ID = "SELECT * FROM clients c LEFT JOIN role r ON r.role_id=c.role_id"
            + " WHERE client_id = ?;";
    private static final String SQL_CREATE_CLIENT = "INSERT INTO clients (client_login, client_f_name, client_l_name,"
            + " client_discount, client_pass, role_id) VALUES (?, ?, ?, ?, sha1(?), ?);";
    private static final String SQL_UPDATE_CLIENT = "UPDATE clients SET client_login=?, client_f_name = ?,"
            + " client_l_name=?, client_discount=? WHERE client_id = ?;";
    private static final String SQL_DELETE_CLIENT = "DELETE FROM clients WHERE client_id = ?";
    private static final String SQL_RESET_PASSWORD = "UPDATE clients SET client_pass = sha1(?) WHERE client_id = ?;";
    private static final String SQL_CHANGE_ROLE = "UPDATE clients SET role_id = ? WHERE client_id = ?;";
    private static final String SQL_GET_ALL_ROLES = "SELECT role_id, role_name FROM role;";
    private static final String SQL_ADD_COMMENT = "INSERT INTO reviews(client_id, review, date) VALUES(?, ?, curdate());";

    private AbstractConnectionPool<ConnectionProxy> pool;
    private ConnectionProxy connection;

    private AdminDAO() {
        pool = ConnectionPoolImpl.getInstance();
    }

    public static AdminDAO getInstance() {
        return DAO;
    }

    @Override
    public List<User> getAllUsers() throws DAOTechnicalException {
        connection = pool.getConnection();
        List<User> users = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_CLIENTS);
            while (resultSet.next()) {
                users.add(buildUser(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL exception(request or table failed). Cannot get users.", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
        return users;
    }

    @Override
    public void addComment(int id, String comment) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_ADD_COMMENT);
            statement.setInt(1, id);
            statement.setString(2, comment);
            statement.executeUpdate();
            LOGGER.info("User Id: " + id + "left the comment");
        } catch (SQLException e) {
            throw new DAOModificationException();
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void addUser(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_CREATE_CLIENT);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getName());
            statement.setString(3, user.getLastName());
            statement.setFloat(4, user.getDiscount()); //default 0 if not set.
            statement.setString(5, user.getPassword());
            if (user.getRole().getId() != 0) {
                statement.setInt(6, user.getRole().getId());
            } else {
                statement.setInt(6, 4);
            }
            statement.executeUpdate();
            LOGGER.info("User " + user.getName() + " " + user.getLastName() + "("
                    + user.getRole().getName() + ")" + " created.");
        } catch (SQLException e) {
            throw new DAOModificationException("User " + user.getLogin() + " not created", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void deleteUser(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_CLIENT);
            statement.setInt(1, user.getId());
            statement.executeUpdate();
            LOGGER.info("User id= " + user.getId() + " successfully deleted.");
        } catch (SQLException e) {
            throw new DAOModificationException("User " + user.getLogin() + " not deleted", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void editUser(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_CLIENT);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getName());
            statement.setString(3, user.getLastName());
            statement.setFloat(4, user.getDiscount());
            statement.setInt(5, user.getId());
            statement.executeUpdate();
            LOGGER.info("User " + user.getName() + " " + user.getLastName() + " successfully changed.");
        } catch (SQLException e) {
            throw new DAOModificationException("User " + user.getLogin() + " not changed", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public User getUser(User user) throws DAOTechnicalException, UserAuthenticationException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_GET_CLIENT_BY_ID);
            statement.setInt(1, user.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return buildUser(resultSet);
            } else {
                throw new UserAuthenticationException("User not found.");
            }
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL exception(request or table failed). Cannon get user.", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void updatePassword(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_RESET_PASSWORD);
            statement.setString(1, user.getPassword());
            statement.setInt(2, user.getId());
            statement.executeUpdate();
            LOGGER.info("Password for " + user.getLogin() + " successfully changed.");
        } catch (SQLException e) {
            throw new DAOModificationException("Password for " + user.getLogin() + " not changed", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public List<Role> getRoles() throws DAOTechnicalException {
        List<Role> roles = new ArrayList<>();
        connection = pool.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_ROLES);
            while (resultSet.next()) {
                roles.add(buildRole(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOTechnicalException("SQL exception(request or table failed). Cannot get roles", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
        return roles;
    }

    @Override
    public void changeRole(User user) throws DAOTechnicalException {
        connection = pool.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_CHANGE_ROLE);
            statement.setInt(1, user.getRole().getId());
            statement.setInt(2, user.getId());
            statement.executeUpdate();
            LOGGER.info("Role of " + user.getName() + " " + user.getLastName() + " successfully changed.");
        } catch (SQLException e) {
            throw new DAOModificationException("Role of" + user.getLogin() + " not changed", e);
        } finally {
            closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public Person authenticateUser(String login, String pass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Appointment showAllAppointments(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Abonement> showAllAbonnements(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Review> showAllReviews() throws DAOTechnicalException {
        return CommonDAOImpl.getInstance().showAllReviews();
    }

    @Override
    public List<Trainer> showAllTrainers() throws DAOTechnicalException {
        return CommonDAOImpl.getInstance().showAllTrainers();
    }

    /**
     * @param resultSet should be positioned on required row
     * @throws SQLException could be thrown also in case of empty result set
     */
    private User buildUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(COLUMN_CLIENT_ID));
        user.setLogin(resultSet.getString(COLUMN_CLIENT_LOGIN));
        user.setName(resultSet.getString(COLUMN_CLIENT_F_NAME));
        user.setLastName(resultSet.getString(COLUMN_CLIENT_L_NAME));
        user.setDiscount(resultSet.getFloat(COLUMN_CLIENT_DISCOUNT));
        user.setTrainer(resultSet.getInt(COLUMN_CLIENT_TRAINER_ID));
        user.setRole(buildRole(resultSet));
        return user;
    }

    private Role buildRole(ResultSet resultSet) throws SQLException {
        return new Role.Builder()
                .withId(resultSet.getInt(COLUMN_ROLE_ID))
                .withName(resultSet.getString(COLUMN_ROLE_NAME))
                .build();
    }

    private void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Statement not closed:" + e);
        }
    }
}
