package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.Abonement;
import com.hutsko.fitnesscentre.entity.Appointment;
import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;

import java.util.List;

public interface PersonDAO extends CommonDAO{

    List<Abonement> showAllAbonnements(User user);
    Appointment showAllAppointments(String name);

    List<User> getAllUsers() throws DAOTechnicalException;

    List<Role> getRoles() throws DAOTechnicalException;

    User getUser(User user) throws DAOTechnicalException, UserAuthenticationException;

    //todo move out this method to clientDAO ?
    void addComment(int id, String comment) throws DAOTechnicalException;

    void deleteUser(User user) throws DAOTechnicalException;

    void editUser(User user) throws DAOTechnicalException;

    void updatePassword(User user) throws DAOTechnicalException;

    void changeRole(User user) throws DAOTechnicalException;

}