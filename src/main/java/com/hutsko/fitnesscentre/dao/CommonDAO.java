package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.Person;
import com.hutsko.fitnesscentre.entity.Review;
import com.hutsko.fitnesscentre.entity.Trainer;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.exception.UserAuthenticationException;

import java.util.List;

public interface CommonDAO {
    /**
     * @param login - user login.
     * @param pass  - user password.
     * @return <code>User</code> with such login and password from database.
     * @throws UserAuthenticationException if a user with such login and password wasn't found in database.
     * @throws DAOTechnicalException       if a database access error occurs or this method was called on a closed
     *                                     connection.
     */
    Person authenticateUser(String login, String pass) throws UserAuthenticationException, DAOTechnicalException;

    void addUser(User user) throws DAOTechnicalException;

    List<Review> showAllReviews() throws DAOTechnicalException;

    List<Trainer> showAllTrainers() throws DAOTechnicalException;

}
