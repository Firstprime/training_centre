package com.hutsko.fitnesscentre.pool;

import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOConnectionException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.ResourceBundle;
import java.util.TimeZone;

public class ConnectionPoolImplTest {
    private static ResourceBundle resource = ResourceBundle.getBundle("mysql_config");
    private  ConnectionPoolImpl<ConnectionProxy> pool;
    private ConnectionProxy connection;


    private final static String SQL_CHECK_USER_2 =
            "SELECT role_id, client_f_name FROM clients " +
                    "WHERE client_login = 'gytko_family@tut.by' AND client_pass = sha(1234567890)";
    @BeforeClass
    public  void prepareTests(){
        pool = ConnectionPoolImpl.getInstance();
    }
    @AfterClass
    public void closeResources(){
        pool.terminate();
    }

    @Test
    public void isPoolSingletonTest(){
        int old = System.identityHashCode(pool);
        ConnectionPoolImpl<ConnectionProxy> newPool = ConnectionPoolImpl.getInstance();
        int newest = System.identityHashCode(newPool);
        Assert.assertEquals(old, newest, newest);
    }

    @Test(expectedExceptions = ClassCastException.class)
    public void putForeignConnectionInPoolTest(){
        String url = resource.getString("db.url");
        String user = resource.getString("db.username");
        String pass = resource.getString("db.password");
        int poolSize = Integer.parseInt(resource.getString("db.poolsize"));
        ConnectionProxy proxy = null;
        Connection connection;
        try {
            proxy = pool.getConnection();
            System.out.println(pool.getPoolSize());
            connection = DriverManager.getConnection(url, user, pass);
            pool.returnToPool((ConnectionProxy) connection); //attempt to put a foreign connection into pool
            Assert.assertEquals(poolSize-1, pool.getPoolSize());
        } catch (DAOConnectionException e) {
            Assert.fail("DAOConnectionException occurred");
        } catch (SQLException e) {
            Assert.fail("SQLException occurred");
        }finally {
            pool.returnToPool(proxy);
        }
    }

    @Test
    public void isConnectionValidTest() {
        try {
            connection = pool.getConnection();
            Assert.assertTrue(pool.isValid(connection));
        } catch (DAOConnectionException e) {
            Assert.fail("Inner application exception occurred");
        } finally {
            pool.returnConnection(connection);
        }
    }

    @Test
    public void poolSizeTest() {
        int poolSize = Integer.parseInt(resource.getString("db.poolsize"));
        Assert.assertEquals(poolSize, pool.getPoolSize(), pool.getPoolSize());
    }

    @Test
    public void simpleQueryTest() {
        User user;
        Statement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.createStatement();
            user = new User();
            ResultSet resultSet = statement.executeQuery(SQL_CHECK_USER_2);
            while (resultSet.next()) {
                user.setName(resultSet.getString(2));
            }
            System.out.println(user);
            Assert.assertNotNull(user.getName());
        } catch (Exception e) {
            Assert.fail("Exception occurred");
        } finally {
            closeStatement(statement);
            pool.returnToPool(connection);
        }
    }

    @Test(enabled = false)
    public void timeZoneTest(){
        TimeZone zone = TimeZone.getDefault();
        System.out.println("ID = " + zone.getID());
        System.out.println("offset = " + zone.getOffset(System.currentTimeMillis()));
        System.out.println("DSTSaving = " + zone.getDSTSavings());
        System.out.println("DayLightTime = " + zone.useDaylightTime());
        System.out.println("zone = " + zone);
    }

    public static void closeStatement(Statement statement) {
        try {
            if (statement == null) {
                System.out.println("statement is null");
            } else
                statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
