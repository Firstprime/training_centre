package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.Role;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImpl;
import com.hutsko.fitnesscentre.pool.ConnectionProxy;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class AdminDAOTest {
    private  ConnectionPoolImpl<ConnectionProxy> pool;
    private AdminDAO adminDAO;

    @BeforeTest
    public  void prepareTest(){
        pool = ConnectionPoolImpl.getInstance();
        adminDAO = AdminDAO.getInstance();
    }

    @AfterClass
    public void closeResources(){
        pool.terminate();
    }

    @Test
    public void getRolesTest(){
        int currentRolesSize = 4;
        List<Role> roles;
        try {
            roles = adminDAO.getRoles();
            Assert.assertEquals(currentRolesSize, roles.size());
        } catch (DAOTechnicalException e) {
            Assert.fail("DAOTechnicalException occurred");
        }
    }
}