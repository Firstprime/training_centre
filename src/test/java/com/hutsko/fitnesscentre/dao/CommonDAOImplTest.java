package com.hutsko.fitnesscentre.dao;

import com.hutsko.fitnesscentre.entity.Review;
import com.hutsko.fitnesscentre.entity.User;
import com.hutsko.fitnesscentre.exception.DAOTechnicalException;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImpl;
import com.hutsko.fitnesscentre.pool.ConnectionPoolImplTest;
import com.hutsko.fitnesscentre.pool.ConnectionProxy;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class CommonDAOImplTest {
    private ConnectionPoolImpl<ConnectionProxy> pool;
    private CommonDAOImpl dao;
    private ConnectionProxy connection;
    private final static String SQL_CHECK_USER =
            "SELECT role_id, client_f_name FROM clients WHERE client_login = ? AND client_pass = sha(?);";
    private final static String SQL_CHECK_USER_2 =
            "SELECT role_id, client_f_name FROM clients " +
                    "WHERE client_login = 'gytko_family@tut.by' AND client_pass = sha1(1234567890)";

    @BeforeClass
    public  void prepareTest(){
        pool = ConnectionPoolImpl.getInstance();
        dao = CommonDAOImpl.getInstance();
    }
    @AfterClass
    public void closeResources(){
        pool.terminate();
    }

    @Test
    public void showAllReviewsTest(){
        List<Review> reviews;
        try {
            reviews = dao.showAllReviews();
            System.out.println(reviews.size());
            Assert.assertNotEquals(reviews, 0);
        } catch (DAOTechnicalException e) {
            Assert.fail("DAOTechnicalException occurred");
        }
    }

    @Test
    public void authorizeUserTest() {
        User user;
        try {
            user = (User)dao.authenticateUser("gytko_family@tut.by", "1234567890");
            System.out.println(user);
            Assert.assertNotNull(user.getName());
        } catch (Exception e) {
            Assert.fail("Exception occurred");
        }
    }

    @Test
    public void preparedStatementSelectUserTest(){
        User user;
        PreparedStatement pStatement = null;
        try {
            connection = pool.getConnection();
            pStatement = connection.prepareStatement(SQL_CHECK_USER);
            pStatement.setString(1, "gytko_family@tut.by");
            pStatement.setString(2, "1234567890");
            user = new User();
            ResultSet resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
//                user.setRole(resultSet.getString(1));
                user.setName(resultSet.getString(2));
            }
            System.out.println(user);
            Assert.assertEquals("Евгений", user.getName());
        }catch (Exception e){
            Assert.fail("Exception occurred");
        } finally {
            try {
                if (pStatement == null) {
                    System.out.println("preparedStatement is null");
                }else
                pStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    @Test
    public void SelectUserTest(){
        User user;
        Statement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.createStatement();
            user = new User();
            ResultSet resultSet = statement.executeQuery(SQL_CHECK_USER_2);
            while (resultSet.next()) {
                user.setName(resultSet.getString("client_f_name"));
            }
            System.out.println(user);
            Assert.assertEquals("Евгений", user.getName());
        } catch (Exception e) {
            Assert.fail("SQLException occurred");
        } finally {
            ConnectionPoolImplTest.closeStatement(statement);
            pool.returnConnection(connection);
        }
    }

}