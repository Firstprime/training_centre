package com.hutsko.fitnesscentre.validator;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class UserValidatorTest {

    private static final String PASSWORD_GOOD = "justsomeletters";
    private static final String PASSWORD_GOOD_2 = "@&%$)@#!%$^fjJD142";
    private static final String PASSWORD_GOOD_LONG = "45charslength34567890123456789012345678901234";
    private static final String PASSWORD_BAD = "русскиепаролинельзя";
    private static final String PASSWORD_BAD_SPACE = "1234 567890";
    private static final String PASSWORD_BAD_SHORT = "less8";
    private static final String PASSWORD_BAD_LONG = "46charslength345678901234567890123456789012345";
    private static final String LOGIN_GOOD = "Firstprime85@gmail.com";
    private static final String LOGIN_GOOD_2 = "gytko_family@tut.by";
    private static final String LOGIN_GOOD_3 = "gytko_family@tut.four";
    private static final String LOGIN_GOOD_LONG = "45rissertanlymuchmorethe1234n@rtyfivesym.true";
    private static final String LOGIN_BAD = "Русская@почта.што";
    private static final String LOGIN_BAD_2 = "@anything.net";
    private static final String LOGIN_BAD_SPACE = "with space@mail.ru";
    private static final String LOGIN_BAD_LONG = "thereissertanlymuchmorethen@rtyfivesymbols.try";

    private static UserValidator validator;

    @BeforeClass
    public static void prepareTest() {
        validator = new UserValidator();
    }

    @AfterClass
    public static void closeResources() {
        validator = null;
    }

    @Test
    public void isLogPassValidTest() {
        boolean answer = validator.isLogPassValid(LOGIN_GOOD, PASSWORD_GOOD);
        Assert.assertTrue(answer);
    }

    @Test
    public void isLogPassValidTest2() {
        boolean answer = validator.isLogPassValid(LOGIN_BAD, PASSWORD_GOOD);
        Assert.assertFalse(answer);
    }

    @Test
    public void isLogPassValidTest3() {
        boolean answer = validator.isLogPassValid(LOGIN_GOOD, PASSWORD_BAD);
        Assert.assertFalse(answer);
    }

    @Test
    public void isLogPassValidTest4() {
        boolean answer = validator.isLogPassValid(LOGIN_BAD, PASSWORD_BAD);
        Assert.assertFalse(answer);
    }

    @Test
    public void isPositivePassValidTest() {
        boolean answer = validator.isPasswordValid(PASSWORD_GOOD);
        Assert.assertTrue(answer);
    }

    @Test
    public void isPositivePassValidTest2() {
        boolean answer = validator.isPasswordValid(PASSWORD_GOOD_2);
        Assert.assertTrue(answer);
    }

    @Test
    public void isPositivePassValidTest3() {
        boolean answer = validator.isPasswordValid(PASSWORD_GOOD_LONG);
        Assert.assertTrue(answer);
    }

    @Test
    public void isNegativePassValidTest() {
        boolean answer = validator.isPasswordValid(PASSWORD_BAD);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativePassValidTest2() {
        boolean answer = validator.isPasswordValid(PASSWORD_BAD_SHORT);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativePassValidTest3() {
        boolean answer = validator.isPasswordValid(PASSWORD_BAD_LONG);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativePassValidTest4() {
        boolean answer = validator.isPasswordValid(PASSWORD_BAD_SPACE);
        Assert.assertFalse(answer);
    }

    @Test
    public void isPositiveLoginValidTest() {
        boolean answer = validator.isLoginValid(LOGIN_GOOD);
        Assert.assertTrue(answer);
    }

    @Test
    public void isPositiveLoginValidTest2() {
        boolean answer = validator.isLoginValid(LOGIN_GOOD_2);
        Assert.assertTrue(answer);
    }

    @Test
    public void isPositiveLoginValidTest3() {
        boolean answer = validator.isLoginValid(LOGIN_GOOD_3);
        Assert.assertTrue(answer);
    }

    @Test
    public void isPositiveLoginValidTest4() {
        boolean answer = validator.isLoginValid(LOGIN_GOOD_LONG);
        Assert.assertTrue(answer);
    }

    @Test
    public void isNegativeLoginValidTest() {
        boolean answer = validator.isLoginValid(LOGIN_BAD);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativeLoginValidTest2() {
        boolean answer = validator.isLoginValid(LOGIN_BAD_2);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativeLoginValidTest3() {
        boolean answer = validator.isLoginValid(LOGIN_BAD_LONG);
        Assert.assertFalse(answer);
    }

    @Test
    public void isNegativeLoginValidTest4() {
        boolean answer = validator.isLoginValid(LOGIN_BAD_SPACE);
        Assert.assertFalse(answer);
    }
}