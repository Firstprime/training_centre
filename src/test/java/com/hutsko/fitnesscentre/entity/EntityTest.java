package com.hutsko.fitnesscentre.entity;

import org.testng.annotations.Test;

public class EntityTest {

    @Test(expectedExceptions = IllegalStateException.class )
    public void createUserTest() { //test new logic of role creating
        User user = new User();
        user.setRole(new Role.Builder().withId(5).withName("testRole").build());
        user.setId(1);
        user.setName("name");
        user.setLastName("lastName");
        user.setLogin("login");
        user.setPassword("pass");
        user.setTrainer(7);
        user.setDiscount(0.5f);
        System.out.println("first iteration: " + user);

        // user.setRole(new Role());                                        // forbidden by private constructor
        user.setRole(new Role.Builder().withId(6).build());                 // throws IllegalStateException
        user.setRole(new Role.Builder().withName("roleName").build());      // throws IllegalStateException
        user.setRole(new Role.Builder().build());                           // throws IllegalStateException

        user.setRole(new Role.Builder().withName("stranger").withId(10).build());
        System.out.println("changed rode: " + user);
    }
    @Test
    public static void getRoleFromUserTest(){
        User user = new User();
        user.setRole(new Role.Builder().withId(5).withName("guest").build());
        System.out.println(user);
        String role = user.getRole().getName();
        System.out.println(role);
    }
}