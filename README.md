Project	EPM-RDBY, "Fitness-centre".

This is a client-server application implementing authorization procedure, a role-based behavior, CRUD operations with a 
database, filters, localization and a connection pool.
The application imitates a fitness-centre site.
	The system operating with two user groups(User and Admin implemented).
	Unauthorized guest can watch list of trainers and reviews, sigh up, change interface language. After registration guest gets 
	user-status.
	User in addition to this can visit Profile-page and change his personal data(not implemented).
	Admin in addition to this can visit Administrate-page and add, delete or change users. Their passwords and roles.

Team: Solo project.

Database: MySQL

WebServer: Apache Tomcat.

Languages: Java, JavaScript, HTML, SQL.

Tools: Maven, TestNG\JUnit, Log4j

Technologies: Servlets, JSP.

Used patterns:
MVC, DAO, Command, Singleton, Proxy, Strategy, Builder.