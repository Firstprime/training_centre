CREATE DATABASE IF NOT EXISTS `project_kachalocka` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `project_kachalocka`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: project_kachalocka
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `abonement_activity`
--

DROP TABLE IF EXISTS `abonement_activity`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonement_activity` (
  `abon_activity_id` INT(11) NOT NULL,
  `abon_activity`    VARCHAR(30) DEFAULT NULL,
  PRIMARY KEY (`abon_activity_id`),
  UNIQUE KEY `abon_term_UNIQUE` (`abon_activity`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abonement_activity`
--

LOCK TABLES `abonement_activity` WRITE;
/*!40000 ALTER TABLE `abonement_activity`
  DISABLE KEYS */;
INSERT INTO `abonement_activity` VALUES (3, 'SPA-complex'), (2, 'Бассейн'), (1, 'Тренажерный зал');
/*!40000 ALTER TABLE `abonement_activity`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abonement_term`
--

DROP TABLE IF EXISTS `abonement_term`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonement_term` (
  `abon_term_id` INT(11)                 NOT NULL
  COMMENT 'PK, FK-Тип абонемента соответстующий данной цене.',
  `abon_term`    SMALLINT(4)             NOT NULL
  COMMENT 'Срок действия абонемента.\n1- разовый,\n31- месячный \n365- годовой.',
  `abon_price`   DECIMAL(11, 2) UNSIGNED NOT NULL
  COMMENT 'Стоимость абонемента в белорусских рублях.  Достаточно DECIMAL(6.2), но учитывая белорускую инфляцию, диапазон расширен до DECIMAL(11.2)  - максимум для 5 байт.',
  PRIMARY KEY (`abon_term_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abonement_term`
--

LOCK TABLES `abonement_term` WRITE;
/*!40000 ALTER TABLE `abonement_term`
  DISABLE KEYS */;
INSERT INTO `abonement_term`
VALUES (1, 1, 5.00), (2, 8, 40.00), (3, 12, 55.00), (4, 16, 70.00), (5, 30, 90.00), (6, 365, 900.00), (7, 1, 10.00),
  (8, 1, 12.00);
/*!40000 ALTER TABLE `abonement_term`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abonements`
--

DROP TABLE IF EXISTS `abonements`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonements` (
  `abons_id`    INT(11)          NOT NULL AUTO_INCREMENT
  COMMENT 'PK - счётчик абонементов.',
  `client_id`   INT(10) UNSIGNED NOT NULL
  COMMENT 'FK - ссылается на клиента купившего абонемент.',
  `abons_start` DATE                      DEFAULT NULL
  COMMENT 'Дата активации абонемента. Хранится в формате yyyy-mm-dd.\n',
  `abons_end`   DATE                      DEFAULT NULL
  COMMENT 'Дата окончания абонемента. Хранится в формате yyyy-mm-dd.\n',
  `abon_id`     INT(11)                   DEFAULT NULL
  COMMENT 'fk- позволяет вычислить тип абонемента.',
  PRIMARY KEY (`abons_id`),
  KEY `idx_client_id` (`client_id`),
  KEY `fk_abon_id_idx` (`abon_id`),
  CONSTRAINT `client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_abon_id` FOREIGN KEY (`abon_id`) REFERENCES `abonement_term` (`abon_term_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 32
  DEFAULT CHARSET = utf8
  COMMENT ='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abonements`
--

LOCK TABLES `abonements` WRITE;
/*!40000 ALTER TABLE `abonements`
  DISABLE KEYS */;
INSERT INTO `abonements` VALUES (1, 1, '2017-12-03', '2017-12-03', 1), (2, 1, '2017-12-03', '2017-12-01', 5),
  (3, 2, '2017-11-01', '2018-11-01', 6), (4, 3, '2017-10-14', '2017-11-14', 2), (5, 4, '2016-05-23', '2016-05-23', 1),
  (6, 6, '2017-09-12', '2017-09-12', 1), (7, 6, '2017-09-15', '2017-09-15', 1), (8, 6, '2017-09-18', '2017-09-18', 1),
  (9, 6, '2017-09-22', '2017-09-22', 1), (10, 7, '2014-03-06', '2015-03-06', 6), (11, 7, '2015-03-06', '2016-03-06', 6),
  (12, 7, '2016-03-06', '2017-03-06', 6), (13, 7, '2017-03-06', '2018-03-06', 6),
  (14, 8, '2017-09-02', '2018-09-02', 6), (15, 8, '2016-09-02', '2017-09-02', 6),
  (16, 8, '2015-09-02', '2016-09-02', 6), (17, 9, '2017-10-01', '2017-11-01', 2),
  (18, 10, '2017-12-03', '2018-12-03', 6), (19, 11, '2017-12-03', '2018-12-03', 6),
  (20, 12, '2017-12-03', '2018-12-03', 6), (21, 13, '2017-12-03', '2018-01-03', 5),
  (22, 14, '2017-12-03', '2018-01-03', 5), (23, 15, '2017-12-03', '2018-01-03', 5),
  (24, 16, '2017-12-03', '2018-01-03', 5), (25, 17, '2017-12-03', '2018-01-03', 5),
  (26, 18, '2017-12-03', '2018-01-03', 5), (27, 19, '2017-12-03', '2018-01-03', 5),
  (28, 20, '2017-12-03', '2018-01-03', 5), (29, 21, '2017-12-03', '2018-01-03', 5),
  (30, 22, '2017-12-03', '2018-01-03', 5), (31, 23, '2017-12-03', '2018-01-03', 5);
/*!40000 ALTER TABLE `abonements`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `allabonements`
--

DROP TABLE IF EXISTS `allabonements`;
/*!50001 DROP VIEW IF EXISTS `allabonements`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `allabonements` AS
  SELECT
    1 AS `Activity`,
    1 AS `abon_term_id`,
    1 AS `term`,
    1 AS `price` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `app_id`       INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
  COMMENT 'PK - счётчик назначений.',
  `client_id`    INT(10) UNSIGNED NOT NULL
  COMMENT 'FK - указывает на конкретного клиента, с которым ассоциирована запись.',
  `app_exercise` TEXT COMMENT 'Хранит программу тренировок клиента (упражнения - подходы - повторы).\n\n',
  `app_diet`     TEXT COMMENT 'Хранит предписания тренера по питанию клиента.',
  `trainer_id`   INT(11)          NOT NULL
  COMMENT 'FK - ссылается на тренера сделавшего назначение.',
  PRIMARY KEY (`app_id`),
  KEY `client_id_idx` (`client_id`),
  CONSTRAINT `fk_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 13
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments`
  DISABLE KEYS */;
INSERT INTO `appointments`
VALUES (1, 9, 'Две инъекции синтола в голову ежедневно! И подъём штанги на бицепс 10подходов х 100повторений', NULL, 0),
  (2, 1, 'подтягивания, подпрыгивания, подталкивания', 'таблетки от больной фантазии', 1),
  (3, 15, 'Приседания, выпады, пресс', '', 8), (4, 20, 'Приседания, выпады, пресс', '', 13),
  (5, 22, 'Приседания, выпады, пресс', '', 13), (6, 23, 'Приседания, выпады, пресс', '', 13),
  (7, 13, 'Жимы, тяги, и т.д.', '', 8), (8, 14, 'Жимы, тяги, и т.д.', '', 8), (9, 16, 'Жимы, тяги, и т.д.', '', 8),
  (10, 17, 'Жимы, тяги, и т.д.', '', 8), (11, 19, 'Жимы, тяги, и т.д.', '', 8), (12, 21, 'Жимы, тяги, и т.д.', '', 9);
/*!40000 ALTER TABLE `appointments`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `client_id`       INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
  COMMENT 'PK - счётчик клиентов.',
  `trainer_id`      INT(10) UNSIGNED          DEFAULT NULL
  COMMENT 'FK - указывает какой тренер приписан к данному клиенту. Поле может быть пустым(нету персонального тренера).',
  `client_f_name`   VARCHAR(45)      NOT NULL
  COMMENT 'Имя клиента. Обязательно для заполнения.',
  `client_l_name`   VARCHAR(45)      NOT NULL
  COMMENT 'Фамилия клиента. Обязательно для заполнения.',
  `client_discount` DECIMAL(3, 2)    NOT NULL DEFAULT '1.00'
  COMMENT 'Скидка клиента - это множитель на который умножается стоимость абонемента.  Т.е. число от 0 до 1. \nПример:  n * 0.85 = скидка 15%',
  `client_login`    VARCHAR(45)      NOT NULL,
  `client_pass`     VARCHAR(45)               DEFAULT NULL
  COMMENT 'Пароль для авторизации клиента в системе. Назначается пользователем через личный кабинет. По-умолчанию пароля нет.',
  `role_id`         INT(11)                   DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `client_login_UNIQUE` (`client_login`),
  UNIQUE KEY `client_login` (`client_login`),
  KEY `index_trainer_id` (`trainer_id`)
    COMMENT 'Индексация по тренеру - для быстрого поиска всех клиентов конкретного тренера.',
  KEY `role_id` (`role_id`),
  CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  CONSTRAINT `idx_trainer_id` FOREIGN KEY (`trainer_id`) REFERENCES `trainers` (`trainer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 120
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients`
  DISABLE KEYS */;
INSERT INTO `clients`
VALUES (1, 1, 'Евгений', 'Гутько', 1.00, 'gytko_family@tut.by', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 2),
  (2, 3, 'Игорь', 'Блинов', 0.90, 'ihar_blinou@epam.com', 'b0399d2029f64d445bd131ffaa399a42d2f8e7dc', 4),
  (3, NULL, 'Саша', 'Курицын', 1.00, 'sasha', NULL, 4), (4, 5, 'Петя', 'Дубина', 1.00, 'petya', NULL, 4),
  (5, 4, 'Вася', 'Просто', 1.00, 'vasya', NULL, 4), (6, NULL, 'Костя', 'Добыш', 1.00, 'kostya', NULL, 4),
  (7, 12, 'Ирина', 'Анищенко', 0.70, 'irina', NULL, 4), (8, 13, 'Татьяна', 'Минкевич', 0.70, 'tanya', NULL, 4),
  (9, NULL, 'Кирилл', 'Терешин', 1.00, 'kirill', NULL, 4), (10, 7, 'Игорь', 'Назаров', 0.85, 'ihar', NULL, 4),
  (11, 7, 'Павел', 'Шагойко', 0.85, 'pavel', NULL, 4), (12, 7, 'Аркадий', 'Добкин', 0.85, 'arkady', NULL, 4),
  (13, 8, 'Дмитрий', 'Ничипоренко', 0.85, 'anihilator', NULL, 4),
  (14, 8, 'Дмитрий', 'Сазонов', 0.85, 'Lumbersexual', NULL, 4),
  (15, 8, 'Анастасия', 'Андрухович', 0.85, 'adorable', NULL, 4),
  (16, 8, 'Дмитрий', 'Холодок', 0.85, 'whiteWalker', NULL, 4), (17, 8, 'Егор', 'Пьянков', 0.85, 'starkiller', NULL, 4),
  (18, 13, 'Елена', 'Плешевич', 0.85, 'joyous', NULL, 4), (19, 8, 'Илья', 'Щербаков', 0.85, 'hunter', NULL, 4),
  (20, 13, 'Анастасия', 'Хапаева', 0.85, 'amusing', NULL, 4),
  (21, 9, 'Валерий', 'Сизоненко', 0.85, 'destroyer', NULL, 4), (22, 13, 'Ирина', 'Вакульчик', 0.85, 'lovely', NULL, 4),
  (23, 13, 'Татьяна', 'Гурская', 0.85, 'bright', NULL, 4), (55, NULL, '2', '3', 1.00, '1', NULL, 4),
  (78, NULL, 'Игорь', 'Гутько', 0.10, 'Origin', '32c8bbff09c356265a96fb8385cfa141c9d92f76', 1),
  (84, NULL, 'must', 'work', 1.00, 'Ibelieve@ican.fly', '1234567890', 4),
  (87, NULL, 'Idonthave', 'fantasy', 1.00, 'nofantasy@mail.da', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 4),
  (88, NULL, 'pustoi', 'baraban', 0.90, 'pustoi@baraban.da', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 4),
  (109, NULL, 'didIfix', 'It', 1.00, 'nullpointer@exception.net', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 4),
  (118, NULL, 'тестируем', 'логи', 1.00, 'tomcat@8_5_2.org', NULL, 4);
/*!40000 ALTER TABLE `clients`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `review_id` INT(11)          NOT NULL AUTO_INCREMENT
  COMMENT 'PK - счётчик отзывов.',
  `client_id` INT(10) UNSIGNED NOT NULL
  COMMENT 'FK - указывает на клиента оставившего отзыв.',
  `review`    VARCHAR(1000)    NOT NULL,
  `date`      DATE             NOT NULL
  COMMENT 'Дата создания отзыва в формате yyyy-mm-dd. Позволяет сортировать отзывы одного клиента по времени добавления.\nкоманда на добавление текущей даты CURRENT_DATE().',
  PRIMARY KEY (`review_id`),
  UNIQUE KEY `review` (`review`),
  KEY `client_id_idx` (`client_id`),
  CONSTRAINT `client_i` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 17
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews`
  DISABLE KEYS */;
INSERT INTO `reviews`
VALUES (1, 1, 'Мой зал - мои правила!', '2017-12-03'), (2, 3, 'Отличный зал, абсолютли!', '2017-12-03'),
  (3, 12, 'Был однажды, приятное место. Пожалуй, куплю себе такой же.', '2017-12-03'),
  (5, 14, 'Не очень понравилось: ниодного барбершопа поблизости и со своим топором не пускают.', '2017-12-03'),
  (6, 10, 'Спасибо, отлично! Лучшее творение из всего что я видел. Это 12 из 10 однозначно!', '2017-12-03'),
  (7, 15, 'Я очень люблю тренирваться под музыку, но со своим роялем не пускают. В остальном миленько)', '2017-12-03'),
  (8, 21, 'Тренировался там пока у меня мотоцикл не угнали. Говорят, какой-то бородатый мужик с топором', '2017-12-03'),
  (9, 19, 'Хороший зал, регулярно ходил, но потом мне прострелили колено', '2017-12-03'),
  (10, 13, 'Тепло, есть удобный диван где можно работать за ноутбуком.', '2017-12-03'),
  (11, 20, 'тестовый отзыв', '2018-01-19'), (15, 20, 'тостовый отзыв', '2018-01-28');
/*!40000 ALTER TABLE `reviews`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id`   INT(11)     NOT NULL,
  `role_name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`role_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role`
  DISABLE KEYS */;
INSERT INTO `role` VALUES (1, 'overlord'), (2, 'admin'), (3, 'trainer'), (4, 'client');
/*!40000 ALTER TABLE `role`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `term_m2m_activity`
--

DROP TABLE IF EXISTS `term_m2m_activity`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `term_m2m_activity` (
  `abon_term_id`  INT(11) NOT NULL
  COMMENT 'PK - однозначно определяет тип абонемента.',
  `abon_activity` INT(11) NOT NULL,
  PRIMARY KEY (`abon_term_id`, `abon_activity`),
  KEY `fk_abon_activ_id_idx` (`abon_activity`),
  CONSTRAINT `fk_abon_activ_id` FOREIGN KEY (`abon_activity`) REFERENCES `abonement_activity` (`abon_activity_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_abon_term_id` FOREIGN KEY (`abon_term_id`) REFERENCES `abonement_term` (`abon_term_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `term_m2m_activity`
--

LOCK TABLES `term_m2m_activity` WRITE;
/*!40000 ALTER TABLE `term_m2m_activity`
  DISABLE KEYS */;
INSERT INTO `term_m2m_activity` VALUES (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1), (7, 2), (8, 3);
/*!40000 ALTER TABLE `term_m2m_activity`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id`    INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(127)     DEFAULT NULL,
  `pass`  VARCHAR(64)      DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test`
  DISABLE KEYS */;
INSERT INTO `test` VALUES (1, 'a', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b');
/*!40000 ALTER TABLE `test`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainers`
--

DROP TABLE IF EXISTS `trainers`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainers` (
  `trainer_id`     INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
  COMMENT 'PK - счётчик тренеров работающих в фитнес-центре.',
  `trainer_f_name` VARCHAR(45)      NOT NULL
  COMMENT 'Имя тренера. Обязательно к заполнению.\n',
  `trainer_l_name` VARCHAR(45)      NOT NULL
  COMMENT 'Фамилия тренера. Обязательно к заполнению.',
  `trainer_login`  VARCHAR(45)      NOT NULL,
  `trainer_pass`   VARCHAR(45)               DEFAULT NULL
  COMMENT 'Пароль для авторизации тренера в системе. Назначается пользователем через личный кабинет. По-умолчанию пароля нет.',
  `trainer_salary` DECIMAL(10, 0) UNSIGNED   DEFAULT NULL
  COMMENT 'Месячная зарплата тренера.',
  `role_id`        INT(11)          NOT NULL DEFAULT '3',
  PRIMARY KEY (`trainer_id`),
  UNIQUE KEY `trainer_login_UNIQUE` (`trainer_login`),
  UNIQUE KEY `trainer_login` (`trainer_login`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `trainers_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainers`
--

LOCK TABLES `trainers` WRITE;
/*!40000 ALTER TABLE `trainers`
  DISABLE KEYS */;
INSERT INTO `trainers`
VALUES (1, 'Chuck', 'Norris', 'login', NULL, 700, 3), (3, 'Arnold', 'Schwarzenegger', 'arnold', NULL, 1000, 3),
  (4, 'Sylvester', 'Stallone', 'sylvester', NULL, 700, 3), (5, 'Dolph', 'Lundgren', 'dolph', NULL, 800, 3),
  (6, 'Bruce', 'Willis', 'bruce', NULL, 900, 3), (7, 'Jackie', 'Chan', 'jackie', NULL, 950, 3),
  (8, 'Jet', 'Li', 'jet', NULL, 850, 3), (9, 'Steven', 'Seagal', 'steven', NULL, 750, 3),
  (10, 'Ronda', 'Rousey', 'кonda', NULL, 750, 3), (12, 'Ronda', 'Rousey', 'ronda', NULL, 750, 3),
  (13, 'Zuzana', 'Light', 'zuzana', NULL, 650, 3);
/*!40000 ALTER TABLE `trainers`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `allabonements`
--

/*!50001 DROP VIEW IF EXISTS `allabonements`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */
  /*!50013 DEFINER =`root`@`localhost`
  SQL SECURITY DEFINER */
  /*!50001 VIEW `allabonements` AS
  SELECT
    `aa`.`abon_activity` AS `Activity`,
    `at`.`abon_term_id`  AS `abon_term_id`,
    `at`.`abon_term`     AS `term`,
    `at`.`abon_price`    AS `price`
  FROM ((`term_m2m_activity` `m2m`
    JOIN `abonement_activity` `aa`) JOIN `abonement_term` `at`)
  WHERE ((`m2m`.`abon_activity` = `aa`.`abon_activity_id`) AND (`at`.`abon_term_id` = `m2m`.`abon_term_id`))
  ORDER BY `at`.`abon_price` */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2018-02-12  2:22:51
